import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';
import { Tools } from '../tools';
import { HTTP } from '@ionic-native/http/ngx';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  deviceInfo;
  bacisAuth;
  options;
  constructor(public auth: AuthGuard,private http: HTTP, public tool: Tools) {
    this.deviceInfo = this.getDeviceInfo();
    this.bacisAuth = 'Basic ' + btoa(environment.username + ":" + environment.password);
    this.options = {
      'X-AGENT-AWAY-API-KEY': environment.apikey,
      'Authorization': this.bacisAuth,
     };
    this.setHeaderData();
  }
  getDate(datetime ){

    console.log('Date Time 0 ',datetime)
    // console.log('Date Time 1 ',new Date(datetime).toTimeString())
    // console.log('Date Time 2 ',new Date(datetime).toUTCString())
    // console.log('Date Time 3 ',new Date(datetime).toLocaleString())
    
    // let dateObject = moment(datetime, "MMM dd, hh:mm a").toDate();
    
    // console.log('Date Time 3 ',dateObject)
    // return new Date(datetime).toUTCString();
    return datetime;
    // return this.commonService.setIonicDateTime(date+' '+time);
   
  }

  setHeaderData() {
    if (this.auth.canActivate && this.getLoginToken() && this.getUserId()) {
      console.log('User Info ', this.getLoginToken());
      console.log('User Info ', this.getUserId());
      this.options = {
        'X-AGENT-AWAY-API-KEY': environment.apikey,
        'Authorization': this.bacisAuth,
        'X-AGENT-AWAY-LOGIN-TOKEN': this.getLoginToken(),
        'USER-ID': this.getUserId()
      }
    }
  }


  login(data): any {
    console.log(' ==> Params =>  ',data);
    return this.http.post(environment.BaseUrl + 'auth/login', data, this.options);
  }
  getCityState(): any {
    console.log(' ==> City State ');
    return this.http.get(environment.BaseUrl + 'services/city', {}, this.options);
  }
  inviteFriends(data): any {
    return this.http.post(environment.BaseUrl + 'Sms', data, this.options);
  }
  getTypeUserList(type): any {
    console.log(' ==> ',type);
    return this.http.get(environment.BaseUrl + 'teamlist/'+type, {}, this.options);
  }
  register(data): any {
    console.log(' register ==> ',data);
    return this.http.post(environment.BaseUrl + 'auth/register', data, this.options);
  }
  userData(userId): any {
    this.setHeaderData();
    console.log(' register ==> ',userId);
    // http://192.168.0.109/ci/agent_away/api/v1/auth/user_data
    return this.http.post(environment.BaseUrl + 'auth/user_data', { id: userId}, this.options);
  }
  userDetails(): any {
    this.setHeaderData();
    this.http.get(environment.BaseUrl + 'auth/user_details', {}, this.options).then(data => {
      console.log('Get User Data ',JSON.parse(data.data).data.user);
      this.setUserData('',this.getLoginToken())  
      this.setUserData(JSON.parse(data.data).data.user,this.getLoginToken());
    }).catch(error => {
      console.log(error);
    });
  //  return ;
  }
  agentList(): any {
    return this.http.get(environment.BaseUrl + 'myAgent/myagent_list', {}, this.options);
  }
  pendingMessages(data): any {
    console.log(data);
    return this.http.post(environment.chatUrl+'/api/chat/PendingMessages', data, this.options);
  }
 agentUpdate(data): any {
  console.log('agent data ', data);
     return this.http.post(environment.BaseUrl + 'myAgent/myagent_list', data, this.options);
 }
  updateProfile(data): any {
    return this.http.post(environment.BaseUrl + 'auth/update_profile', data, this.options);
  }
  send_otp(data): any {
    console.log(' ==> ',data);
    return this.http.post(environment.BaseUrl + 'auth/send_otp', data, this.options);
  }
  forgotPassword(data): any {
    console.log(' ==> ',data);
    return this.http.post(environment.BaseUrl + 'auth/forgot_password', data, this.options);
  }
  forgotPasswordNew(data): any {
    console.log(' ==> ',data);
    return this.http.post(environment.BaseUrl + 'auth/send_otp_forget', data, this.options);
  }
  changePassword(data): any {
    console.log(' ==> ',data);
    return this.http.post(environment.BaseUrl + 'auth/change_password', data, this.options);
  }
  getServiceList(): any {
    this.setHeaderData();
   return this.http.get(environment.BaseUrl + 'services/services_list', {}, this.options);
  }
  getUserDetailsById(): any {
    this.setHeaderData();
   return this.http.get(environment.BaseUrl + 'auth/user_data', {}, this.options);
  }
  getPlan(): any {
    this.setHeaderData();
   return this.http.post(environment.BaseUrl + 'planlist/plan', {PlanType:this.getUserData().role_id}, this.options);
  }
  getPlanHistory(): any {
    this.setHeaderData();
   return this.http.get(environment.BaseUrl + 'planhistory/plan_history', {}, this.options);
  }
  makePayment(data): any {
    return this.http.post(environment.stripeChargeUrl, data, {'Authorization': 'Bearer '+environment.secretkey});
  }

  subScribePlan(data): any {
    console.log(' ==> ',data);
    return this.http.post(environment.BaseUrl + 'planhistory/subscribe', data, this.options);
  }

  Contactus(data): any {
    this.setHeaderData();
   return this.http.post(environment.BaseUrl + 'Contactus', data, this.options);
  }
  createTask(data): any {
    this.setHeaderData();
    console.log(' ==> ',data);
   return this.http.post(environment.BaseUrl + 'task/task_add', data, this.options);
  }
  addBanking(data): any {
    this.setHeaderData();
    console.log('Banking Details Add');
    return this.http.post(environment.BaseUrl + 'banking/add_banking', data, this.options);
  }
  updateBanking(data): any {
    this.setHeaderData();
    console.log('Banking Details Update');
   return this.http.post(environment.BaseUrl + 'banking/update_banking', data, this.options);
  }
  bankDetails(): any {
    this.setHeaderData();
   return this.http.get(environment.BaseUrl + 'banking/bank_detail', {}, this.options);
  }
  homeList(): any {
    this.setHeaderData();
   return this.http.get(environment.BaseUrl + 'task/get_task_list', {}, this.options);
  }
  myPostedTask(): any {
    this.setHeaderData();
    console.log('My PostedTask '+environment.BaseUrl + 'task/my_task_list');
   return this.http.get(environment.BaseUrl + 'task/my_task_list', {}, this.options);
  }
  myAcceptedTask(): any {
    this.setHeaderData();
    console.log('My Accepted '+environment.BaseUrl + 'task/my_accepted_task');
   return this.http.get(environment.BaseUrl + 'task/my_accepted_task', {}, this.options);
  }
  myPostedTaskHistory(): any {
    this.setHeaderData();
    console.log('My PostedTask History '+environment.BaseUrl + 'task/my_task_list_history');
   return this.http.get(environment.BaseUrl + 'task/my_task_list_history', {}, this.options);
  }
  myAcceptedTaskHistory(): any {
    this.setHeaderData();
    console.log('My Accepted History '+environment.BaseUrl + 'task/my_accepted_task_history');

   return this.http.get(environment.BaseUrl + 'task/my_accepted_task_history', {}, this.options);
  }

  PaymentRecived(): any {
    this.setHeaderData();
    console.log('PaymentRecived '+environment.BaseUrl + 'task/PaymentRecived');
   return this.http.get(environment.BaseUrl + 'task/PaymentRecived', {}, this.options);
  }
  PaidOut(): any {
    this.setHeaderData();
    console.log('PaidOut '+environment.BaseUrl + 'task/PaidOut');
   return this.http.get(environment.BaseUrl + 'task/PaidOut', {}, this.options);
  }


  getPaymentTransactions(): any {
    this.setHeaderData();
    console.log('My Accepted History '+environment.BaseUrl + 'task/my_payment_transcation');

   return this.http.get(environment.BaseUrl + 'task/my_payment_transcation', {}, this.options);
  }



  acceptTask(data): any {
    this.setHeaderData();
   return this.http.post(environment.BaseUrl + 'task/acceptedby', data, this.options);
  }
  confirmTask(data): any {
    this.setHeaderData();
   return this.http.post(environment.BaseUrl + 'task/conform_task', data, this.options);
  }
  agreeTask(data): any {
    this.setHeaderData();
   return this.http.post(environment.BaseUrl + 'task/agree_task', data, this.options);
  }
  completeTask(data): any {
    this.setHeaderData();
   return this.http.post(environment.BaseUrl + 'task/complete_task', data, this.options);
  }


  clientSurvey(data): any {
    this.setHeaderData();
   return this.http.post(environment.BaseUrl + 'task/ClientSurvey', data, this.options);
  }
  taskRating(data): any {

    console.log('Task Rating --> ',data);
    this.setHeaderData();
   return this.http.post(environment.BaseUrl + 'task/reting', data, this.options);
  }

  getCurrentLatLng() {
    navigator.geolocation.getCurrentPosition((res) => {
      let currentLatLng: any = {};
      currentLatLng.latitude = res.coords.latitude;
      currentLatLng.longitude = res.coords.longitude;
      return currentLatLng;
    });
  }

  // GET & SET USER DATA
  setUserData(userData, login_token) {
    console.log('Set New Data ',userData,login_token)
    window.localStorage.setItem('user_data', JSON.stringify(userData));
    if (login_token != '')
      window.localStorage.setItem('login_token', login_token);
    window.localStorage.setItem('user_id', userData.id);
  }

  setTempRegister(item) {
    window.localStorage.setItem('reg_data', JSON.stringify(item));
  }
  getTempRegister() {
    if (window.localStorage['reg_data']) {
      return JSON.parse(window.localStorage['reg_data']);
    }
    return;
  }
  selectedTask(item) {
    if (item != null)
      window.localStorage.setItem('selTask', JSON.stringify(item));
    else
      window.localStorage.setItem('selTask', '');
  }
  getSelectedTask() {
    if (window.localStorage['selTask']) {
      return JSON.parse(window.localStorage['selTask']);
    }
    return;
  }
  setTypeUSerList(item) {
    if (item != null)
      window.localStorage.setItem('setTypeUSerList', JSON.stringify(item));
    else
      window.localStorage.setItem('setTypeUSerList', '');
  }
  getTypeUSerList() {
    if (window.localStorage['setTypeUSerList']) {
      return JSON.parse(window.localStorage['setTypeUSerList']);
    }
    return;
  }
  getUserData() {
    if (window.localStorage['user_data']) {
      return JSON.parse(window.localStorage['user_data']);
    }
    return;
  }
  getUserId() {
    if (window.localStorage['user_id']) {
      return window.localStorage['user_id'];
    }
    return;
  }

  getLoginToken() {
    if (window.localStorage['login_token']) {
      return window.localStorage['login_token'];
    }
    return;
  }


  // GET & SET DEVICE INFO
  setDeviceInfo(deviceInfo) {
    window.localStorage.setItem('deviceInfo', JSON.stringify(deviceInfo));
  }

  getDeviceInfo() {
    if (window.localStorage['deviceInfo']) {
      return JSON.parse(window.localStorage['deviceInfo']);
    }
    return;
  }
}
