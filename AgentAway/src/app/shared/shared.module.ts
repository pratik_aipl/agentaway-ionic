import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuthGuard } from './authguard.service';
import { Tools } from '../tools';
import { DatePipe } from '@angular/common';



@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    HttpClientModule,
    ReactiveFormsModule,
    
    ],
  exports: [CommonModule, IonicModule,  ReactiveFormsModule],
  providers: [AuthGuard, Tools, DatePipe]
})
export class SharedModule { }
