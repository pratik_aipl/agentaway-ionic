import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/authguard.service';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register-type', loadChildren: './pages/register-type/register-type.module#RegisterTypePageModule' },
  { path: 'forgot-password', loadChildren: './pages/forgot-password/forgot-password.module#ForgotPasswordPageModule' },
  { path: 'forgot-otp/:mobileno/:otp', loadChildren: './pages/forgot-password/forgot-otp/forgot-otp.module#ForgotOtpPageModule' },
  { path: 'forgot-change-password/:mobileno', loadChildren: './pages/forgot-password/forgot-change-password/forgot-change-password.module#ForgotChangePasswordPageModule' },
  { path: 'register/:selectedtype', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'otpvarify/:mobileno/:otp', loadChildren: './pages/otpvarify/otpvarify.module#OtpvarifyPageModule' },
  { path: 'dashboard',canActivate: [AuthGuard], loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule' },
  { path: 'myprofile', canActivate: [AuthGuard],loadChildren: './pages/dashboard/menu/myprofile/myprofile.module#MyprofilePageModule' },
  { path: 'invite-friend', canActivate: [AuthGuard],loadChildren: './pages/dashboard/menu/invite-friend/invite-friend.module#InviteFriendPageModule' },
  //{ path: 'my-agent', canActivate: [AuthGuard],loadChildren: './pages/my-agent/my-agent.module#MyAgentPageModule' },
  //{ path: 'settings',canActivate: [AuthGuard], loadChildren: './pages/settings/settings.module#SettingsPageModule' },
  { path: 'addtask/:from', canActivate: [AuthGuard],loadChildren: './pages/addtask/addtask.module#AddtaskPageModule' },
  { path: 'task-details/:from/:page', canActivate: [AuthGuard],loadChildren: './pages/dashboard/task-details/task-details.module#TaskDetailsPageModule' },
  // { path: 'message-details', canActivate: [AuthGuard],loadChildren: './pages/dashboard/chat/message-details/message-details.module#MessageDetailsPageModule' },
  // { path: 'settings', loadChildren: './pages/dashboard/menu/settings/settings.module#SettingsPageModule' },
  // { path: 'subscribe', loadChildren: './pages/dashboard/menu/subscribe/subscribe.module#SubscribePageModule' },
  { path: 'client-servey/:id/:agentID/:type', loadChildren: './pages/dashboard/myacceptedtask/client-servey/client-servey.module#ClientServeyPageModule' },
  { path: 'bank-details/:from', loadChildren: './pages/bank-details/bank-details.module#BankDetailsPageModule' },
  // { path: 'history', loadChildren: './pages/dashboard/history/history.module#HistoryPageModule' },
  // { path: 'currently', loadChildren: './pages/dashboard/currently/currently.module#CurrentlyPageModule' },
  // { path: 'faq', loadChildren: './pages/dashboard/menu/faq/faq.module#FaqPageModule' },
  // { path: 'contact-us', loadChildren: './pages/dashboard/menu/contact-us/contact-us.module#ContactUsPageModule' },
  //{ path: 'agent-details', loadChildren: './pages/my-agent/agent-details/agent-details.module#AgentDetailsPageModule' },
  //{ path: 'change-password', loadChildren: './pages/dashboard/menu/change-password/change-password.module#ChangePasswordPageModule' },
];

@NgModule({
  
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
