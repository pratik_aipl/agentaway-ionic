import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { NavController, Events } from '@ionic/angular';

@Component({
  selector: 'app-addtask',
  templateUrl: './addtask.page.html',
  styleUrls: ['./addtask.page.scss'],
})
export class AddtaskPage implements OnInit {

  task: any;
  from: any;
  btnClick = false;
  PageTitle: any;
  btnTitle: any;

  // addTaskForm: FormGroup;
  zipcodes: any = [];
  serviceList: any = [];
  myDate: String = new Date().toISOString();
  user: any;
  ClientName = '';
  ClientMobileNo = '';
  email = '';
  Date = '';
  Time = '';
  SelectTask = '';
  Deal = '';
  Comments = '';
  public asyncValidators = [this.validateAsync];
  public validators = [this.startsWithAt];
  constructor(public router: Router,
    private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,
    public events: Events,
    public commonService: CommonService,
    public tools: Tools, private navController: NavController, ) {
    this.from = this.activatedRoute.snapshot.paramMap.get('from');
    if (this.from == 'edit') {
      this.PageTitle = "Edit Task"
      this.btnTitle = "Update Task"
      this.task = this.commonService.getSelectedTask();
      var szipcodes = this.task.ServiceZipCodes.split(",")
      console.log('ass ==> ', szipcodes.length);
      for (let i = 0; i < szipcodes.length; i++) {
        const element = szipcodes[i];
        console.log('ass ==> ', element);
        this.zipcodes.push({ display: element, value: element });
      }
      this.ClientName = this.task.ClientName;
      this.ClientMobileNo = this.task.ClientMobileNo;
      this.email = this.task.ClientEmail;//, [Validators.required, Validators.email.bind(this)]],
      this.Date = this.task.DateTime;
      this.Time = this.task.DateTime;
      this.SelectTask = this.task.ServiceID;
      this.Deal = this.task.Deal;
      this.Comments = this.task.Comments;
    } else {
      this.PageTitle = "Add Task"
      this.btnTitle = "Add Task"
    }


  }
  private validateAsync(control: FormControl): Promise<any> {
    return new Promise(resolve => {
      const value = control.value;
      const result: any = isNaN(value) ? {
        isNan: true
      } : null;

      setTimeout(() => {
        resolve(result);
      }, 1);
    });
  }
  private startsWithAt(control: FormControl) {
    if (control.value.length > 5) {
      return {
        'startsWithAt@': true
      };
    }

    return null;
  }

  public asyncErrorMessages = {
    'startsWithAt@': 'zip code length maximum 5 digits',
    isNan: 'Please only add numbers'
  };

  ngOnInit() {

  }
  ionViewDidEnter() {
    this.commonService.userDetails();
    this.serviceData();
  }
  onChangeDate($event) {
    // console.log('Change Date', $event);
    // console.log('Change Date', $event.target);
  }
  onChangeTime($event) {
    // console.log('Change Time', $event);
    // console.log('Change Time', $event.target);
  }


  onSubmit() {
    var msg = ''
    if (this.Date == '') {
      msg = msg + 'Please select Date.<br />';
    } if (this.Time == '') {
      msg = msg + 'Please select time.<br />';
    } if (this.SelectTask == '') {
      msg = msg + 'Please select task type.<br />';
   // } if (this.ClientName == '') {
   //   msg = msg + 'Please enter client name.<br />';
   // } if (this.ClientMobileNo == '') {
   //    msg = msg + 'Please enter client mobile no.<br />';
  //  } if (this.email == '') {
  //   msg = msg + 'Please enter valid email.<br />';
    } if (this.zipcodes.length == 0) {
      msg = msg + 'Please enter zip code Areas of interest.<br />';
    } if (this.Deal == '') {
      msg = msg + 'Please enter deal amount.<br />';
    } if (this.Comments == '') {
      msg = msg + 'Please enter comment.';
    }
    if (msg != '') {
      this.tools.openAlert(msg);
    } else {
      var selectDate = this.Date.split('T')[0]
      var selectTime = this.Time.split('T')[1]
      selectTime = selectTime.substring(0, 8)
      // console.log('form selectDate ', selectDate);
      // console.log('form selectTime ', selectTime);
      if (this.from == 'edit') {
        var form1 = {
          TaskID: this.task.TaskID,
          ClientName: this.ClientName,
          ClientMobileNo: this.ClientMobileNo.replace('-','').replace('-',''),
          email: this.email,
          ServiceZipCodes: Array.prototype.map.call(this.zipcodes, function (item) { return item.value; }).join(","),
          Date: selectDate,
          Time: selectTime,
          Time1: this.Time,
          SelectTask: this.SelectTask,
          // Total: this.Total,
          Deal: this.Deal,
          Comments: this.Comments,
        }
        console.log('Updated Data', form1);
        this.tools.openLoader();
        this.commonService.createTask(form1).then(data => {
          this.tools.closeLoader();
          this.ClientName = '';
          this.ClientMobileNo = '';
          this.email = '';
          this.Date = '';
          this.Time = '';
          this.SelectTask = '';
          this.Deal = '';
          this.Comments = '';
          this.zipcodes = [];
          this.events.publish('taskUpdate');
          this.navController.navigateRoot('/dashboard/tabs/mypostedtask');

        }).catch(error => {
          console.log(error);
          this.tools.closeLoader();
          if (error && error.error) {
            this.tools.openAlert(JSON.parse(error.error).message);
          }
        });
      } else {
        this.user = this.commonService.getUserData();
        console.log('User Data ', this.ClientMobileNo)
        console.log('User Data ', this.ClientMobileNo.replace('-',''))
        if (this.user.BankingStatus == undefined || this.user.BankingStatus == "0") {
          this.tools.updateBankingDetails('Please update banking details.', 'Ok')
        }else {
          var form2 = {
            ClientName: this.ClientName,
            ClientMobileNo:  this.ClientMobileNo.replace('-','').replace('-',''),
            email: this.email,
            ServiceZipCodes: Array.prototype.map.call(this.zipcodes, function (item) { return item.value; }).join(","),
            Date: selectDate,
            Time: selectTime,
            Time1: this.Time,
            SelectTask: this.SelectTask,
            // Total: this.Total,
            Deal: this.Deal,
            Comments: this.Comments,
          }
          if (!this.btnClick) {
            this.tools.openLoader();
            this.btnClick = true;

            if (this.commonService.getUserData().available_tasks > 0 && this.commonService.getUserData().available_tasks != 0) {
              this.commonService.createTask(form2).then(data => {
                this.btnClick = false;
                this.tools.closeLoader();
                //   this.addTaskForm.reset();
                this.ClientName = '';
                this.ClientMobileNo = '';
                this.email = '';
                this.Date = '';
                this.Time = '';
                this.SelectTask = '';
                this.Deal = '';
                this.Comments = '';
                this.zipcodes = [];

                var user = this.commonService.getUserData();
                user.available_tasks = "" + (parseInt(this.commonService.getUserData().available_tasks) - 1)
                this.commonService.setUserData(user, '');
                this.navController.navigateRoot('/dashboard/tabs/mypostedtask');
              }).catch(error => {
                console.log(error);
                this.btnClick = false;
                this.tools.closeLoader();
                if (error && error.error) {
                  if (JSON.parse(error.error).member_limit) {
                    if (JSON.parse(error.error).available_tasks_limit == 1) {
                      this.tools.presentAwailableTask(JSON.parse(error.error).message, 'Subscribe', 'Cancel')
                    } else {
                      this.tools.openAlert(JSON.parse(error.error).message);
                    }
                  } else {
                    this.tools.openAlert(JSON.parse(error.error).message);
                  }
                }
              });
            }
          }
        }

      }
    }
  }

  serviceData() {
    this.tools.openLoader();
    this.commonService.getServiceList().then(data => {
      this.tools.closeLoader();
      this.serviceList = JSON.parse(data.data).data.services_list;
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }

}
