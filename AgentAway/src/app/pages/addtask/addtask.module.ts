import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TagInputModule } from 'ngx-chips';

import { AddtaskPage } from './addtask.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';

const routes: Routes = [
  {
    path: '',
    component: AddtaskPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    NgxMaskIonicModule,
     TagInputModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddtaskPage]
})
export class AddtaskPageModule {}
