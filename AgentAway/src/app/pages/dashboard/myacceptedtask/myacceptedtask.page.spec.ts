import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyacceptedtaskPage } from './myacceptedtask.page';

describe('MyacceptedtaskPage', () => {
  let component: MyacceptedtaskPage;
  let fixture: ComponentFixture<MyacceptedtaskPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyacceptedtaskPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyacceptedtaskPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
