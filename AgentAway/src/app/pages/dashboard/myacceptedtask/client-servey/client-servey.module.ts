import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StarRatingModule } from 'ionic4-star-rating';
import { ClientServeyPage } from './client-servey.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: ClientServeyPage
  }
];

@NgModule({
  imports: [
    SharedModule,FormsModule,
    StarRatingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ClientServeyPage]
})
export class ClientServeyPageModule {}
