import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-client-servey',
  templateUrl: './client-servey.page.html',
  styleUrls: ['./client-servey.page.scss'],
})
export class ClientServeyPage implements OnInit {

  registerForm: FormGroup;
  pageTitle=''
  clientmeet: any;
  rat:any;
  opt:any;
  TaskID:any;
  type:any;
  agentID:any;

  @ViewChild('rating') rating : any;

  constructor(public router: Router,private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,
    public commonService: CommonService,public events: Events,
    public tools: Tools) { 
   // this.task=this.commonService.getSelectedTask();
    this.type = this.activatedRoute.snapshot.paramMap.get('type');
    this.TaskID = this.activatedRoute.snapshot.paramMap.get('id');
    this.agentID = this.activatedRoute.snapshot.paramMap.get('agentID');
    //    this.registerForm= this.formBuilder.group({
    //     TaskID: [this.TaskID],
    //     agentID: [this.agentID],
    //   q1: ['Did you meet with your assigned agent?',],
    //   a1: ['', Validators.required],
    //   q2: ['How would you rate your agent?',],
    //   a2: ['', Validators.required],
    // });

    if(this.type == 'accept'){
      this.pageTitle="View Survey"
      this.getSurwayDetails();
    }else{
      this.pageTitle="Client Survey"
    }

  }

  logRatingChange(rating) {
    console.log("changed rating: ", rating);
    this.rat = rating;
  }

  selected(value: string) {
    this.clientmeet = value;
    console.log(this.clientmeet);
  }
  getSurwayDetails(){
    this.tools.openLoader();
    var form={TaskID:this.TaskID}
    this.commonService.clientSurvey(form).then(data => {
      this.tools.closeLoader();
      this.tools.closeLoader();

      console.log('Client Servay Details ',JSON.parse(data.data).data);
        this.rat=JSON.parse(data.data).data.ClientSurvey[1].Ans;
        this.opt=JSON.parse(data.data).data.ClientSurvey[0].Ans;
      //   this.registerForm= this.formBuilder.group({
      //     TaskID: [this.TaskID],
      //     agentID: [this.agentID],
      //   q1: ['Did you meet with your assigned agent?',],
      //   a1: [JSON.parse(data.data).data.ClientSurvey[0].Ans, Validators.required],
      //   q2: ['How would you rate your agent?',],
      //   a2: [this.rat, Validators.required],
      // });

    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  onSubmit(){

    var form={
      TaskID:this.TaskID,
      agentID: this.agentID,
      q1: 'Did you meet with your assigned agent?',
      a1: this.opt,
      q2: 'How would you rate your agent?',
      a2: this.rat,
    }


    this.tools.openLoader();
    this.commonService.taskRating(form).then(data => {
      this.tools.closeLoader();
      console.log('Task Confirm ', data);
      this.events.publish('taskUpdate'); 
      this.events.publish('survayUpdate'); 
      this.tools.openAlert(JSON.parse(data.data).message);
      this.router.navigateByUrl('/');
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  ngOnInit() {

  }
}
