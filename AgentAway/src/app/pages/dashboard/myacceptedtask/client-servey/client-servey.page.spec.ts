import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientServeyPage } from './client-servey.page';

describe('MyacceptedtaskPage', () => {
  let component: ClientServeyPage;
  let fixture: ComponentFixture<ClientServeyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientServeyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientServeyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
