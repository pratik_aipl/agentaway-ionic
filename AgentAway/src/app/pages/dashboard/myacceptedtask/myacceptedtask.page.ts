import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-myacceptedtask',
  templateUrl: './myacceptedtask.page.html',
  styleUrls: ['./myacceptedtask.page.scss'],
})
export class MyacceptedtaskPage implements OnInit {
  selectPage:any;
  acceptedTaskList=[];
  toDoHistoryList=[];
  
  constructor(public router: Router,
    public commonService:CommonService,
    public events: Events,
    private tools:Tools) { 
      this.selectPage='post';
    events.subscribe('taskUpdate', (item) => {
      console.log('Event call',item)
      this.taskData(false);
    });
    events.subscribe('refreshCall', (item) => {
      console.log('Event call',item)
      if(item === 'Current Active Tasks')
      this.taskData(true);
    });    

  }

  ngOnInit() {
    
  }
  taskClick(item,type){
    this.commonService.selectedTask(item);
    this.router.navigateByUrl('/task-details/'+type+'/'+type);
  }
  ionViewDidEnter(){
    this.taskData(true); 
  }

  taskData(isShow) {
    if(isShow)
    this.tools.openLoader();
    this.commonService.myAcceptedTask().then(data => {
      this.tools.closeLoader();
      this.acceptedTaskList=[];
      console.log( ' My Accepted Task ',JSON.parse(data.data));
      if(JSON.parse(data.data).data != undefined)
      this.acceptedTaskList = JSON.parse(data.data).data.my_accepted_task;
      console.log( this.acceptedTaskList);
      this.taskDataAccepted(isShow);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
 taskDataAccepted(isShow) {
    if(isShow)
    this.tools.openLoader();

    this.commonService.myAcceptedTaskHistory().then(data => {
      this.tools.closeLoader();
      console.log('Posted Task History ',JSON.parse(data.data).data)
      this.toDoHistoryList=[];
      if(JSON.parse(data.data).data != undefined)
      this.toDoHistoryList = JSON.parse(data.data).data.my_accepted_task;
    //  console.log( this.toDoHistoryList);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  openChat(item,status){
    this.commonService.selectedTask(item);
    this.router.navigateByUrl('dashboard/message-details/'+item.name+'/'+item.CreatedBy+'/list');
  }
  CompleteTask(item,status){
    this.tools.openLoader();
    var form={Complete: status,TaskID:item.TaskID}
    console.log('Task Complete ', form);
    this.commonService.completeTask(form).then(data => {
      console.log('Task Length after ', data);
      this.tools.closeLoader();
      this.tools.openAlert(JSON.parse(data.data).message);
      // for (let i = 0; i < this.acceptedTaskList.length; i++) {
      //   const element = this.acceptedTaskList[i];
      //  if(element.TaskID==item.TaskID){
      //   this.acceptedTaskList[i].Conform=status;
      //   this.acceptedTaskList[i].Complete=status;
      //   this.acceptedTaskList[i].Agree=status;
      //   break;
      //  }
      // }
      this.taskData(false); 
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }

  ConfirmTask(item,status){
    this.tools.openLoader();
    var form={Conform: status,TaskID:item.TaskID}
    this.commonService.confirmTask(form).then(data => {
      this.tools.closeLoader();
      console.log('Task Confirm ', data);
      this.tools.openAlert(JSON.parse(data.data).message);
      // for (let i = 0; i < this.toDoHistoryList.length; i++) {
      //   const element = this.toDoHistoryList[i];
      //  if(element.TaskID==item.TaskID){
      //   this.toDoHistoryList[i].Conform=status;
      //   break;
      //  }
      // }
      this.taskData(false); 
      console.log('Task Length after ', this.toDoHistoryList.length);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  AgreeTask(item,status){
    this.tools.openLoader();
    var form={Agree: status,TaskID:item.TaskID}
    this.commonService.agreeTask(form).then(data => {
      this.tools.closeLoader();
      console.log('Task Agree ', data);
      this.tools.openAlert(JSON.parse(data.data).message);
      // for (let i = 0; i < this.toDoHistoryList.length; i++) {
      //   const element = this.toDoHistoryList[i];
      //  if(element.TaskID==item.TaskID){
      //   this.toDoHistoryList[i].Agree=status;
      //   this.toDoHistoryList[i].Conform=status;
      //   break;
      //  }
      // }
      this.taskData(false);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }

  pay(item){
    this.tools.openAlert('Coming Soon');
    // this.router.navigateByUrl('/client-servey/'+item.TaskID+'/'+item.AcceptedBy+'/'+type);
   }
  ClentSurvey(item,type){
    console.log(' --> ',item.ClientSurvey);
    this.router.navigateByUrl('/client-servey/'+item.TaskID+'/'+item.AcceptedBy+'/'+type);
   }
  viewProfile(id){
    console.log('Task User ID '+id)
    this.router.navigateByUrl('dashboard/profile/'+id);
    }
}
