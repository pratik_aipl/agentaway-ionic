import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SellMyLeadPage } from './sellmylead.page';

const routes: Routes = [
  {
    path: '',
    component: SellMyLeadPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SellMyLeadPage]
})
export class SellMyLeadPageModule {}
