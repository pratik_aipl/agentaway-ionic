import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SellMyLeadPage } from './sellmylead.page';


describe('SellMyLeadPage', () => {
  let component: SellMyLeadPage;
  let fixture: ComponentFixture<SellMyLeadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellMyLeadPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellMyLeadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
