import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { Events } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-sellmylead',
  templateUrl: './sellmylead.page.html',
  styleUrls: ['./sellmylead.page.scss'],
})
export class SellMyLeadPage implements OnInit {
  user: any;
  
  constructor(public router: Router,public commonService:CommonService,private iab:InAppBrowser,
    public events: Events,private tools:Tools) { 
      this.commonService.userDetails();
      this.user = this.commonService.getUserData();
  }

  ionViewDidEnter(){
   
  }

  ngOnInit() {
   
  }



  openPage(){
       const browser = this.iab.create('http://agentawayleads.com' ,'_self');   
  }

  addTask(){
    console.log('activeplan_id ', this.commonService.getUserData().activeplan_id);
    console.log('available_tasks ', this.commonService.getUserData().available_tasks);

    if (this.commonService.getUserData().role_id == 2) {
      if (this.commonService.getUserData().available_tasks > 0) {
        this.router.navigateByUrl('/addtask/add');
      } else {
        this.tools.presentAwailableTask('Available task limit exceeded! Subscribe now? ', 'Subscribe', 'Cancel')
      }
    } else {
      if (this.commonService.getUserData().available_tasks > 0 && this.commonService.getUserData().available_tasks != 0) {
        this.router.navigateByUrl('/addtask/add');
      } else {
        this.tools.presentAwailableTask('Available task limit exceeded! Subscribe now? ', 'Subscribe', 'Cancel')
      }
    }
  }
}
