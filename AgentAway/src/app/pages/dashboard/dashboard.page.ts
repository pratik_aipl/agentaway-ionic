import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  user: any;
  isHome = true;
  isMenu = false;
  tabName = 'Home';
  constructor(public router: Router, public events: Events,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) { }

  ionViewDidEnter() {
    this.commonService.userDetails();
    this.user = this.commonService.getUserData();
    // if(this.user.BankingStatus ==undefined || this.user.BankingStatus == "0" ){
    //   this.tools.updateBankingDetails('Please update banking details.','Ok')
    // }
  }

  ngOnInit() {
  }

  tabChange(event) {
    
    if (event.tab == 'home') {
      this.isMenu=false;
      this.isHome = true
      this.tabName = 'Home';
    } else {
      this.isHome = false;

      if (event.tab == 'myacceptedtask') {
        this.isMenu=false;
        this.tabName = 'Current Active Tasks';
      } else if (event.tab == 'mypostedtask') {
        this.isMenu=false;
        this.tabName = 'Task History';
      } else if (event.tab == 'menu') {
        this.isMenu=true;
        this.tabName = 'Menu';
      }

    }

  }
  callRefressh(){
    this.events.publish('refreshCall',this.tabName); 
  }
  addTask() {
    console.log('activeplan_id ', this.commonService.getUserData().activeplan_id);
    console.log('available_tasks ', this.commonService.getUserData().available_tasks);

    if (this.commonService.getUserData().role_id == 2) {
      if (this.commonService.getUserData().available_tasks > 0) {
        this.router.navigateByUrl('/addtask/add');
      } else {
        this.tools.presentAwailableTask('Available task limit exceeded! Subscribe now? ', 'Subscribe', 'Cancel')
      }
    } else {
      if (this.commonService.getUserData().available_tasks > 0 && this.commonService.getUserData().available_tasks != 0) {
        this.router.navigateByUrl('/addtask/add');
      } else {
        this.tools.presentAwailableTask('Available task limit exceeded! Subscribe now? ', 'Subscribe', 'Cancel')
      }
    }
  }

}
