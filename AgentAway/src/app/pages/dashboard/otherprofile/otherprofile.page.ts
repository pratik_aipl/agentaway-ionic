import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-otherprofile',
  templateUrl: './otherprofile.page.html',
  styleUrls: ['./otherprofile.page.scss'],
})
export class OtherProfilePage implements OnInit {
  isEdit=false;
  businessCard=false;
  userId:any;
  ZipCode:'';
  rat=0;
  user={
    userimage:'',
    name:'',
    license_number:'',
    mobile_no:'',
    email:'',
    rating:0,
    brokeroffice_name:'',
    role_id:'',
    About:'',
    business_card:'',
    DesVehicleSafety:'',
    street_address:'',
    city:'',
    state:'',
    st_zip:'',
    ZiipCode:''
  };
  constructor(public router: Router,private activatedRoute: ActivatedRoute,public commonService: CommonService,public tools: Tools) {
    this.userId = this.activatedRoute.snapshot.paramMap.get('userId');
    this.getUserData();
  }
     


  ngOnInit() {

  }

  getUserData(){
    this.tools.openLoader();
    this.commonService.userData(this.userId).then(data => {
      this.tools.closeLoader();
      this.tools.closeLoader();
       this.user =JSON.parse(data.data).data.user;
       this.rat=this.user.rating;
       this.ZipCode=Array.prototype.map.call(this.user.ZiipCode, function(item) { return item.ZipCode; }).join(",")
    
       console.log('Get User Data ',this.user);
       console.log('this.ZipCode ',this.ZipCode);
    }).catch(error => {
      this.tools.closeLoader();
      this.tools.closeLoader();
      console.log(error);
    });
  }
  imagePreview(imgPAth){
    this.businessCard= !this.businessCard
console.log(imgPAth);
  }
}
