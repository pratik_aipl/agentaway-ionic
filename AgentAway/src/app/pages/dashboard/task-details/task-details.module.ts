import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskDetailsPage } from './task-details.page';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: TaskDetailsPage
  }
];

@NgModule({
  imports: [
   SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TaskDetailsPage]
})
export class TaskDetailsPageModule {}
