import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Tools } from 'src/app/tools';
import { Events } from '@ionic/angular';
import { Socket } from 'ngx-socket-io';
@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.page.html',
  styleUrls: ['./task-details.page.scss'],
})
export class TaskDetailsPage implements OnInit,OnDestroy {

  task:any;
  from:any;
  page:any;
  show=false;
  isAccept=false;
  reciverID=0;
  msgCount:any='0';
  constructor(public router: Router,private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    private socket: Socket,
    public events: Events,
    public tools: Tools) { 
    this.task=this.commonService.getSelectedTask();
    this.from = this.activatedRoute.snapshot.paramMap.get('from');
    this.page = this.activatedRoute.snapshot.paramMap.get('page');

    if(this.from == 'accept'){
      this.reciverID=this.task.CreatedBy;
      if(this.task.Conform != 0){
        this.show =true;
      }else{
        this.show =false;
      }
    }
    console.log(this.from);
    console.log(this.task.AcceptedBy);
    console.log('Selected Task ',this.task);
    if(this.from == 'posted' && this.task.AcceptedBy != '0' ){
      this.show =true;
      this.reciverID=this.task.AcceptedBy;
    }

    events.subscribe('survayUpdate', (item) => {
      console.log('Event call',item)
      this.task.ClientSurvey='1';
    });
    events.subscribe('msgCount', (item) => {
      console.log('Event call',item)
      this.msgCount='0';
    });
  }

  ionViewDidEnter() {
    this.socket.connect();
     const data = {
       senderID: this.commonService.getUserId(),
       receiverID: this.reciverID,
       type: true
   }
    if(this.task.Conform != 0){
    this.socket.emit('set-name', data);
    this.socket.emit("msgCount", { senderID: this.reciverID, receiverID: this.commonService.getUserId(), taskID: this.task.TaskID });
    this.socket.fromEvent('msgCount').subscribe(count => {
        console.log('Message Count --> ',count);
        this.msgCount=count;
      });
    }
  }

  ionViewWillLeave() {
    console.log('ionViewWillLeave ');
  // this.socket.disconnect();
  }
  ngOnInit() {
    // this.socket.connect();
    // if(this.task.Conform != 0){
    //   //  this.socket.emit('set-name', data);
    //   this.socket.fromEvent('msgCount').subscribe(count => {
    //     console.log('Message Count --> ',count);
    //     this.msgCount=count;
    //   });
    // }

  }
  ngOnDestroy(): void {
    this.commonService.selectedTask('');
  }
  openChat(){
    console.log('Message Details');
    this.msgCount='0';
    // if(this.task.Conform != 0){
    // this.socket.emit("msgCount", { senderID: this.reciverID, receiverID: this.commonService.getUserId(), taskID: this.task.TaskID });
    // }
    this.router.navigateByUrl('dashboard/message-details/'+this.task.name+'/'+this.reciverID+'/details');
  }
  goToEdit(){
    console.log('goToEdit');
    this.router.navigateByUrl('/addtask/edit');
  }

    CompleteTask(item,status){
    this.tools.openLoader();
    var form={Complete: status,TaskID:item.TaskID}
    console.log('Task Complete ', form);
    this.commonService.completeTask(form).then(data => {
      console.log('Task Length after ', data);
      this.tools.closeLoader();
      this.tools.openAlert(JSON.parse(data.data).message);
      this.task.Conform=status;
        this.task.Complete=status;
        this.task.Agree=status;
        this.events.publish('taskUpdate'); 
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }


  ConfirmTask(item,status){
    this.tools.openLoader();
    var form={Conform: status,TaskID:item.TaskID}
    this.commonService.confirmTask(form).then(data => {
      this.tools.closeLoader();
      console.log('Task Confirm ', data);
      this.tools.openAlert(JSON.parse(data.data).message);
      this.task.Conform=status;
      this.events.publish('taskUpdate'); 
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  AgreeTask(item,status){
    this.tools.openLoader();
    var form={Agree: status,TaskID:item.TaskID}
    this.commonService.agreeTask(form).then(data => {
      this.tools.closeLoader();
      console.log('Task Agree ', data);
      this.tools.openAlert(JSON.parse(data.data).message);
      this.task.Agree=status;
      this.task.Conform=status;
      this.events.publish('taskUpdate'); 
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  AcceptTask(item,status){
    this.tools.openLoader();
    var form={AcceptedBy: status,TaskID:item.TaskID}
    this.commonService.acceptTask(form).then(data => {
      this.tools.closeLoader();
      this.isAccept=true;
      this.events.publish('taskUpdate'); 
      this.tools.openAlertTODO('Your request to take this task has been sent to the posting agent. They can confirm or deny the task. Please check your TO DO tab');
      // if(status=='1'){        
      // }else{
      //   this.tools.openAlertTODO('Your request to take this task has been sent to the posting agent. They can confirm or deny the task. Please check your TO DO tab');
      // }

    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }

  viewProfile(id){
    console.log('Task User ID '+id)
    this.router.navigateByUrl('dashboard/profile/'+id);
    }
}
