import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  chatList=[];
  constructor(public router: Router,) { 
    // for (let i = 0; i < 10; i++) {
    //   this.chatList[i]="sample ";
      
    // }
  }

  ngOnInit() {
  }
  chatClick(item){
    this.router.navigateByUrl('dashboard/message-details/'+item);
  }
}
