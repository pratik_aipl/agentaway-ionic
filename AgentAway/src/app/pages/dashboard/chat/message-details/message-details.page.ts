import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { ToastController, Events } from '@ionic/angular';
import { CommonService } from 'src/app/shared/common.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/app/tools';
@Component({
  selector: 'app-message-details',
  templateUrl: './message-details.page.html',
  styleUrls: ['./message-details.page.scss'],
})
export class MessageDetailsPage implements OnInit {
  message = '';
  messages = [];
  currentUser = '';
  receiverID: any;
  typing = false;
  task: any;
  user: any;
  from: any;
  page: any;
  topPoint=50;

  @ViewChild('content') private content: any;
  @ViewChild('inMsg') yourElement: ElementRef;

  constructor(private tools: Tools, private socket: Socket,public events: Events, public commonService: CommonService, private activatedRoute: ActivatedRoute, private toastCtrl: ToastController) {
    this.task = this.commonService.getSelectedTask();
    this.page = this.activatedRoute.snapshot.paramMap.get('page');
    this.from = this.activatedRoute.snapshot.paramMap.get('from');
    console.log('Chat Screen ', this.from);
    this.receiverID = this.activatedRoute.snapshot.paramMap.get('type');
    this.user = this.commonService.getUserData();
  }

  
  ngOnInit() {
    this.callgetPendingMessage(true);
    this.socket.connect();
    this.currentUser = this.user.name;
    const data = {
      senderID: this.commonService.getUserId(),
      receiverID: this.receiverID,
      type: true
    }
    if(this.page == 'list')
    this.socket.emit('set-name', data);
    this.socket.fromEvent('typing').subscribe(typing => {
      this.typing = true;
    });
    this.socket.fromEvent('stopTyping').subscribe(stopTyping => {
      console.log('Stop Typing --> ');
      this.typing = false;
    });
    this.socket.fromEvent('chat message').subscribe(message => {
      const msg = {
        SenderId: this.receiverID,
        Message: message,
        UpdatedAt: new Date(),
      }
      this.messages.push(msg);
      this.typing = false;
      if(this.topPoint>60){
        this.scrollToLatestMessage();
      }
      // this.grid.scrollToBottom();
    });


  }

  scrollFunction(ev) {
   this.topPoint=ev.detail.scrollTop
    // console.log('scroll event scrollTop', ev.detail.scrollTop);
    // console.log('scroll event detail', ev.detail);
   }

  callgetPendingMessage(isFill) {
    const data = {
      senderID: this.commonService.getUserId(),
      receiverID: this.receiverID,
      TaskId: this.task.TaskID,
    }
    this.tools.openLoader();
    console.log('callgetPendingMessage ');
    this.commonService.pendingMessages(data).then(data => {
      this.tools.closeLoader();
      console.log('Pending message list --> ', JSON.parse(data.data));
      if(isFill){
        if (JSON.parse(data.data).messages != undefined)
          this.messages = JSON.parse(data.data).messages;
          this.scrollToLatestMessage();
      }
      }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }

  onChangeTime(data): void {
    console.log("onChangeTime to time: " + this.message + ". Event data: " + data);
    this.socket.emit("typing", { msg: this.message, senderID: this.commonService.getUserId(), receiverID: this.receiverID, taskID: this.task.TaskID });
    setTimeout(() => {
      this.socket.emit("stopTyping", { msg: this.message, senderID: this.commonService.getUserId(), receiverID: this.receiverID, taskID: this.task.TaskID });
    }, 1000);
  }

  sendMessage() {
    this.socket.emit("chat message", { msg: this.message, senderID: this.commonService.getUserId(), receiverID: this.receiverID, taskID: this.task.TaskID });
    this.socket.emit("msgCount", { senderID: this.commonService.getUserId(), receiverID: this.receiverID, taskID: this.task.TaskID });
    // this.events.publish('msgCount'); 
    const msg = {
      SenderId: this.commonService.getUserId(),
      Message: this.message,
      UpdatedAt: new Date(),
    }
    this.messages.push(msg);
    this.message = '';
    this.scrollToLatestMessage();
  }

  ionViewWillLeave() {
    console.log('ionViewWillLeave ');
    this.events.publish('msgCount'); 
    this.callgetPendingMessage(false);
  //  this.socket.emit("msgCount", { senderID: this.receiverID, receiverID: this.commonService.getUserId(), taskID: this.task.TaskID });
//    this.socket.disconnect();
  }

  scrollToLatestMessage(): void {
    setTimeout(() => {
      this.content.scrollToBottom(1500);
    }, 300);
  }


  async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
}
