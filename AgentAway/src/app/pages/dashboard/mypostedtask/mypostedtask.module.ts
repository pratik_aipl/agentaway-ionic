import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MypostedtaskPage } from './mypostedtask.page';

const routes: Routes = [
  {
    path: '',
    component: MypostedtaskPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MypostedtaskPage]
})
export class MypostedtaskPageModule {}
