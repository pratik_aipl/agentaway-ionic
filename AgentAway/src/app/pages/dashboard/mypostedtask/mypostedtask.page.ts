import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-mypostedtask',
  templateUrl: './mypostedtask.page.html',
  styleUrls: ['./mypostedtask.page.scss'],
})
export class MypostedtaskPage implements OnInit {
  selectPage:any;
  postedTaskList=[];
  postedHistoryList=[];
  constructor(public router: Router,public commonService:CommonService,
    public events: Events,private tools:Tools) { 
      this.selectPage='post';
    events.subscribe('taskUpdate', (item) => {
      console.log('Event call',item)
      this.taskDataHistory(false);
    });

      events.subscribe('refreshCall', (item) => {
        console.log('Event call',item)
        if(item === 'Task History')
        this.taskDataHistory(true);
      });    
  }

  ionViewDidEnter(){
    this.taskDataHistory(true);
  }

  ngOnInit() {
   
  }

  openChat(item,status){
    this.commonService.selectedTask(item);
    this.router.navigateByUrl('dashboard/message-details/'+item.name+'/'+item.AcceptedBy+'/list');
  }
  
  dispDate(date){
console.log('Sample Date ',date)
// moment(now.format(), moment.ISO_8601).format();
//  return 

  }
  taskDataHistory(isShow) {
    if(isShow)
    this.tools.openLoader();

    this.commonService.myPostedTaskHistory().then(data => {
      this.tools.closeLoader();
      this.postedHistoryList=[];
      console.log('Accepted Task History ',JSON.parse(data.data).data)
      if(JSON.parse(data.data).data != undefined)
      this.postedHistoryList = JSON.parse(data.data).data.services_list;
      console.log( this.postedHistoryList);
      this.taskData(isShow);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }

   taskData(isShow) {
    if(isShow)
    this.tools.openLoader();

    this.commonService.myPostedTask().then(data => {
      this.tools.closeLoader();
      this.postedTaskList=[];
      console.log( ' My Accepted Task ',JSON.parse(data.data));
      if(JSON.parse(data.data).data != undefined)
      this.postedTaskList = JSON.parse(data.data).data.services_list;
      console.log( this.postedTaskList);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }

  

  taskClick(item,type){
    this.commonService.selectedTask(item);
    this.router.navigateByUrl('/task-details/'+type+'/'+type);
  }

  ConfirmTask(item,status){
    this.tools.openLoader();
    var form={Conform: status,TaskID:item.TaskID}
    this.commonService.confirmTask(form).then(data => {
      this.tools.closeLoader();
      console.log('Task Confirm ', data);
      if(status == 0){
        this.router.navigateByUrl('/dashboard/tabs/home');
      }else{
        this.tools.openAlert(JSON.parse(data.data).message);
        this.taskDataHistory(false);
      }
      // for (let i = 0; i < this.postedTaskList.length; i++) {
      //   const element = this.postedTaskList[i];
      //  if(element.TaskID==item.TaskID){
      //   this.postedTaskList[i].Conform=status;
      //   break;
      //  }
      // }
      // console.log('Task Length after ', this.postedTaskList.length);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  AgreeTask(item,status){
    this.tools.openLoader();
    var form={Agree: status,TaskID:item.TaskID}
    this.commonService.agreeTask(form).then(data => {
      this.tools.closeLoader();
      console.log('Task Agree ', data);
      this.tools.openAlert(JSON.parse(data.data).message);
      // for (let i = 0; i < this.postedTaskList.length; i++) {
      //   const element = this.postedTaskList[i];
      //  if(element.TaskID==item.TaskID){
      //   this.postedTaskList[i].Agree=status;
      //   this.postedTaskList[i].Conform=status;
      //   break;
      //  }
      // }
      // this.taskDataHistory(false);
      this.taskDataHistory(false);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  deleteTask(item,status){
    this.tools.openLoader();
    var form={AcceptedBy: status,TaskID:item.TaskID}
    this.commonService.acceptTask(form).then(data => {
      this.tools.closeLoader();
      this.taskDataHistory(false);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  ClentSurvey(item,type){
    this.router.navigateByUrl('/client-servey/'+item.TaskID+'/'+item.AcceptedBy+'/'+type);
   }
   viewProfile(id){
    console.log('Task User ID '+id)
    this.router.navigateByUrl('dashboard/profile/'+id);
    }
}
