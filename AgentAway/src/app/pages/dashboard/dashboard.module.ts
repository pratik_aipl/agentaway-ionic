import { PaymentTransactionsPage } from './menu/payment-transactions/payment-transactions.page';
import { ExpandlistComponent } from './../../widgets/expandlist/expandlist.component';
import { FaqPage } from './menu/faq/faq.page';
import { ContactUsPage } from './menu/contact-us/contact-us.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StarRatingModule } from 'ionic4-star-rating';
import { DashboardPage } from './dashboard.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChangePasswordPage } from './menu/change-password/change-password.page';
import { SettingsPage } from './menu/settings/settings.page';
import { MyAgentPage } from './menu/my-agent/my-agent.page';
import { AgentDetailsPage } from './menu/my-agent/agent-details/agent-details.page';
import { MessageDetailsPage } from './chat/message-details/message-details.page';
import { Stripe } from '@ionic-native/stripe/ngx';
import { SubscribePage } from './menu/subscribe/subscribe.page';
import { FormsModule } from '@angular/forms';
import { PayModalComponent } from './menu/subscribe/pay-model/pay-model.component';
import { OtherProfilePage } from './otherprofile/otherprofile.page';

const routes: Routes = [
  {
    path: "tabs",
    component: DashboardPage,
    children: [
      {
        path: "",
        redirectTo: "tabs/(home:home)",
        pathMatch: "full"
      },
      {
        path: "home",
        children: [
          {
            path: "",
            loadChildren: "../dashboard/home/home.module#HomePageModule"
          }
        ]
      },
      {
        path: "mypostedtask",
        children: [
          {
            path: "",
            loadChildren: "../dashboard/mypostedtask/mypostedtask.module#MypostedtaskPageModule"
          }
        ]
      },
      {
        path: "myacceptedtask",
        children: [
          {
            path: "",
            loadChildren: "../dashboard/myacceptedtask/myacceptedtask.module#MyacceptedtaskPageModule"
          }
        ]
      },
      {
        path: "sellmylead",
        children: [
          {
            path: "",
            loadChildren: "../dashboard/sellmylead/sellmylead.module#SellMyLeadPageModule"
          }
        ]
      },
      {
        path: "chat",
        children: [
          {
            path: "",
            loadChildren: "../dashboard/chat/chat.module#ChatPageModule"
          }
        ]
      },
      {
        path: "menu",
        children: [
          {
            path: "",
            loadChildren: "../dashboard/menu/menu.module#MenuPageModule"
          }
        ]
      }
    ]
  },
  {
    path: "",
    redirectTo: "tabs/home",
    pathMatch: "full"
  },
  {
    path: 'change-password',
    component: ChangePasswordPage
  },
  {
    path: 'profile/:userId',
    component: OtherProfilePage
  },
  {
    path: 'settings',
    component: SettingsPage
  },
  {
    path: 'my-agent',
    component: MyAgentPage
  },
  {
    path: 'agent-details/:agentId',
    component: AgentDetailsPage
  },
  {
    path: 'subscribe',
    component: SubscribePage
  },
  {
    path: 'faq',
    component: FaqPage
  },
  {
    path: 'contact-us',
    component: ContactUsPage
  },
  {
    path: 'payment-transactions',
    component: PaymentTransactionsPage
  },
  {
    path: 'message-details/:from/:type/:page',
    component: MessageDetailsPage
  },
  {
    path: 'pay-modal',
    component: PayModalComponent
  }
];

@NgModule({
  entryComponents: [PayModalComponent],

  imports: [
    SharedModule, FormsModule,
    StarRatingModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    Stripe,
  ],
  declarations: [
    DashboardPage,
    OtherProfilePage,
    MyAgentPage,
    AgentDetailsPage,
    SettingsPage,
    ChangePasswordPage,
    MessageDetailsPage,
    SubscribePage,
    ContactUsPage,
    PaymentTransactionsPage,
    FaqPage,
    PayModalComponent,
      ExpandlistComponent]
})
export class DashboardPageModule { }
