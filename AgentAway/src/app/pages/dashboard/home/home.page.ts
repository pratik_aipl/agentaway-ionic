import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  searchTerm: string = '';
  allData = []; //Store all data from provider
  taskList = [];//Store filtered data
  constructor(public router: Router, public events: Events,private commonService:CommonService,private tools:Tools) { 
    events.subscribe('taskUpdate', (item) => {
      console.log('Event call',item)
      this.taskData(false);
    });
    events.subscribe('refreshCall', (item) => {
      console.log('Event call',item)
      if(item === 'Home')
      this.taskData(true);
    });

  }

  ngOnInit() {
   
  }

  taskClick(item){
    console.log('Task Click ',item);
   this.commonService.selectedTask(item);
   this.router.navigateByUrl('/task-details/home/home');
  }

  ionViewDidEnter(){
    this.taskData(false);
  }

  ionChange() {
    if (this.searchTerm.trim() !== '')
    {
       this.taskList = this.allData.filter((item) =>
       {
         return item.ServiceZipCodes.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || item.ServiceZipCodes.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
       })
    }else{
      this.taskList =this.allData;
    }
  }
  taskData(isShow) {
    if(isShow)
       this.tools.openLoader();
    this.commonService.homeList().then(data => {
      this.tools.closeLoader();
      this.tools.closeLoader();
      this.taskList=[];
      if(JSON.parse(data.data).data != undefined){
        this.taskList = JSON.parse(data.data).data.services_list;
        this.allData = JSON.parse(data.data).data.services_list;
      }
      console.log( this.taskList);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  AcceptTask(item, status) {

    if (this.commonService.getUserData().BankingStatus == 1 || this.commonService.getUserData().BankingStatus == '1') {
      this.tools.openLoader();
      var form = { AcceptedBy: status, TaskID: item.TaskID }
      this.commonService.acceptTask(form).then(data => {
        this.tools.closeLoader();
        console.log('Task Length before', this.taskList.length);
        for (let i = 0; i < this.taskList.length; i++) {
          const element = this.taskList[i];
          if (element.TaskID == item.TaskID) {
            this.taskList.splice(i, 1);
            break;
          }
        }
        console.log('Task Length after ', this.taskList.length);
        this.tools.openAlertTODO('Your request to take this task has been sent to the posting agent. They can confirm or deny the task. Please check your TO DO tab');

      }).catch(error => {
        console.log(error);
        this.tools.closeLoader();
        if (error && error.error) {
          this.tools.openAlert(JSON.parse(error.error).message);
        }
      });
    } else {
      this.router.navigateByUrl('/bank-details/login');
    }
  }
  
}
