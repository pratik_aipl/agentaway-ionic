import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.page.html',
  styleUrls: ['./contact-us.page.scss'],
})
export class ContactUsPage implements OnInit {

  queryForm: FormGroup;
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) {

    this.queryForm = this.formBuilder.group({
      Email: ['', [Validators.required, Validators.email.bind(this)]],
      ContactNo: ['', Validators.required],
      Query: ['', Validators.required]
    });
   }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log('Updated Data',form);
    this.tools.openLoader();
    this.commonService.Contactus(form).then(data => {
      this.tools.closeLoader();
      this.queryForm.reset();
      this.tools.openAlert(JSON.parse(data.data).message);

  }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
}
