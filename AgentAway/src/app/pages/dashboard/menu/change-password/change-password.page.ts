import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, NgForm, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  cpForm: FormGroup;
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) {
      this.cpForm = this.formBuilder.group({
        old_password: ['', [Validators.required]],
        new_password: ['', Validators.required],
        Confirm: ['', Validators.required],
      });
     }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log(form);

    
    if(this.cpForm.get('new_password').value != this.cpForm.get('Confirm').value){
      this.tools.openAlert('New Password And Confirm Password Dont Match.');
    }else if(this.cpForm.get('new_password').value == this.cpForm.get('old_password').value){
      this.tools.openAlert('Old Password And New Password Are Similar.');
    }else{
      this.tools.openLoader();
      this.commonService.changePassword(form).then(data => {
        this.tools.closeLoader();    
        this.cpForm.reset(); 
        this.tools.openAlert(JSON.parse(data.data).message);
  
      }).catch(error => {
        console.log(error);
        this.tools.closeLoader();
        if (error && error.error) {
          this.tools.openAlert(JSON.parse(error.error).message);
        }
      });
    }

    
  }
}
