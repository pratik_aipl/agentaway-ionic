import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage implements OnInit {
  public technologies : Array<{ name: string, description: string }> = [
    {
       name : 'Is AgentAway.com.com a real estate brokerage?',
       description : "No. AgentAway.com.com is not a brokerage and does not employ any sales associates, real estate agents, REALTOR®s or brokers. AgentAway.com is an online platform that connect licensed real estate professionals with other agents to get paid for tasks that will allow one licensee to get paid by another as a paid service for a specific task."
    },
    {
       name : 'Does AgentAway.com.com pay referral fees?',
       description : "No. AgentAway.com.com does not allow referral fees. Referral fees must go through your broker’s office. AgentAway.com.com is a paid service where licensees are paid for a service that is actually performed."
    },
    {
       name : 'Can a member be paid directly?',
       description : "Yes. Licensees can pay other licensees for tasks done that are considered unlicensed activities such as selling leads to other licensees, administrative tasks such as social media assistant, appointment setter and more. Click here for a list of tasks available."
    },
    {
       name : '*How do I get paid for real estate activities that are considered licensed activities?',
       description : "Brokers must give specific consent that allows agents to receive income from the app. Brokers must either sign up as a Broker/Team or email a broker consent. REALTOR®s or broker-associates must give the broker information on all payments received and the broker must keep records for at least 5 years or whatever is required by the state in which he/she does business. Click here for licensed tasks that require Broker’s authorization."
    },
    {
       name : 'How much do I get paid for completing a task?',
       description : "Tasks are recommended to be paid by fair market value. For example, if a paying agent needs to show 5 houses within a 10 mile radius, the paying agent should pay an amount that is consistent with the amount of time the agent should be compensated. Another example, if a paying agent requires a house to be cleaned that would ordinarily cost $150.00, the paying agent can choose to pay that amount, or a higher or lower amount depending on the size and condition of the house. Licensees has the flexibility to pay according to their budget and the skills required for the task. (We recommend payments of $30 or more for any task.)"
    },
    {
       name : 'How do I know how much I will get paid?',
       description : "Once the task notification is sent, the amount in the Deal section is the amount the paying agent will pay the agent for completing that task."
    },
    {
       name : 'How do I place an order?',
       description : "Just sign in and click the + (add) button, located at the top of the left side of the screen, which will allow you to enter information to place an order."
    },
    {
       name : 'What is the difference between the red Buyer’s Agent tasks, red Listing Agent’s Tasks and the blue Administrator’s tasks?',
       description : "All Blue Administrator tasks are tasks that may be recognized as tasks that do not require a license and all payments will be directed to your account. All Red Buyer’s Agent and Listing Agent Tasks may be recognized as real estate activity and may require Broker’s authorization to receive payment. A broker may require payments to go directly to the broker’s office for compliance reasons. <b>Check with your Broker before accepting a task!</b> Or click here and email the Broker’s authorization form and email to the address on the form."
    },
    {
       name : 'How do I accept a task?',
       description : "Once a task is posted by an agent, the agent can release the task to his/her team or to all members immediately or within 24 hours. All members in that area or zip code will receive a notification and can click the accept button. The agent that posted the task have the option to accept that agent or not."
    },
    {
       name : 'What is the cost to post a task?',
       description : "A single post is $19.99. You can purchase a monthly subscription which allows you or your team to communicate within the team or as an individual. Click here for memberships"
    },
    {
       name : 'What’s the difference between signing up as a REALTOR®, Broker or Team?',
       description : "Signing up as a REALTOR® allows you to post or accept tasks independently. Signing up as a Broker or Team allows the owner of the team to accept other members onto a platform where communication will be available only to the team members. You or the team can post tasks to each other on your own platform or to the entire membership."
    },
    {
       name : 'How do I invite a team member?',
       description : "Once you have signed up as a Broker or Team-Owner, go to your settings and send your team member the broker code. This will prompt you to send an invite to your team members, once they sign up and accept, you will be notified of their joining your team. Only one Broker-Owner or Team-Owner is allowed."
    },
    {
       name : 'How do a team owner or I remove myself from a team?',
       description : "You must notify the team owner to be removed from the team. Only the team owner can remove a team member."
    },
    {
       name : 'How do I know a member won’t try to continue working with my customer?',
       description : "Once you have established a relationship with your client, you must inform your client that you will have a licensed agent assist you with the transaction who will only assist with a specific task. Let your client know they are to communicate with you only. Besides, your client already chose to work with you and will love the fact that you have a team willing and ready to assist with their needs! The agent who accepts the job also e-signs a non-compete form so that you’ll feel comfortable knowing that he/she is being paid for that specific task and should not have any further communication with your client."
    },
    {
       name : 'What if an agent continues to communicate with my buyer?',
       description : "You can file a complaint with us through our contact us button and we may terminate their profile from our network after an investigation is completed."
    },
    {
       name : 'When will the receiving agent get paid?',
       description : "After the transaction is completed, the agent listing the task must Release the funds to the receiving agent."
    },
    {
       name : 'What if the receiving agent does not Release the funds?',
       description : "Our sales team will contact the agent. If we are unable to contact the agent and have verified that the job was completed, the receiving agent will receive the funds. If there is a dispute, please write the details in our contact us page."
    },
    {
       name : 'What if I am not satisfied with the agent who completed the task?',
       description : "You can submit a complaint. But if the task was completed, you have the option not to work with that agent again. We cannot control an agents’ behavior but we can exclude one from being part of the network after an investigation is completed."
    },
    {
       name : 'Can I use this service in another County?',
       description : "Yes. This service can be used within the state of Florida."
    },
    {
       name : 'Can I use this service in another state?',
       description : "You must stay in compliance with local and state laws. If you are licensed in one state, you cannot do business in another state without meeting licensure requirements. However, if you are conducting business while you are outside of the state you are licensed in you can conduct business. If you are uncertain, please contact your broker or state in which you are licensed."
    },
    {
       name : 'Can I sell my leads?',
       description : "Yes. Leads can be sold nationally or internationally. Once you have sold a lead you agree that you will no longer contact that lead. If the lead is someone you personally know, you must inform your client that they will be contacted by another REALTOR®. You must also explain if the lead is exclusive or not."
    },
    {
       name : 'What happens if I post a listing and no one responds?',
       description : "You can contact us and we will reach out to agents. If no one is available, your account will be credited and no charge will be made."
    },
    {
       name : 'How do I obtain a refund?',
       description : "Simply go to the contact us page and request a refund along with details and customer service will contact you."
    },
    {
       name : 'Will I receive a 1099 form from AgentAway.com?',
       description : "No. Members are not employed by AgentAway.com.com. Please contact your Broker, CPA or accountant for advice on information about any IRS questions."
    },
    {
       name : 'Why is my Social Security Number needed?',
       description : "Payments to agents will be processed by stripe.com, a software platform. Due to IRS reporting requirements, stripe.com requires your social security number to report income over $600. Stripe may issue a 1099. It ‘s recommended that you consult a tax advisor to determine your tax filing and reporting requirements."
    },
    {
       name : 'Can I write off my posted tasks on IRS forms?',
       description : "Please contact your accountant or CPA."
    }
  ];

  constructor() { }

  ngOnInit() {
  }
  public captureName(event: any) : void
  {
     console.log(`Captured name by event value: ${event}`);
  }
}
