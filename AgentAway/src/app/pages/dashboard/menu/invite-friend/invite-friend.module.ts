import { TagInputModule } from 'ngx-chips';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { InviteFriendPage } from './invite-friend.page';

const routes: Routes = [
  {
    path: '',
    component: InviteFriendPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    TagInputModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InviteFriendPage]
})
export class InviteFriendPageModule {}
