import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-invite-friend',
  templateUrl: './invite-friend.page.html',
  styleUrls: ['./invite-friend.page.scss'],
})
export class InviteFriendPage implements OnInit {
//  inviteForm: FormGroup;
  image1: string='';
  isEdit=false;
  emails= [];
  subject="";
  msg="";
  user:any;
  public validators = [Validators.email.bind(this)];
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) {
      this.user=commonService.getUserData();
      this.emails= [];
      this.subject="Agent Away | Invitation to Join";
      this.msg='Hi,\nI am pleased to invite you to join my team on Agent Away using the code mentioned below.\n\n'+this.user.RoleName+' Code - '+this.user.my_broker_code+'\n\niOS users - https://apps.apple.com/us/app/apple-store/id1477325592 \n\nAndroid users -  https://play.google.com/store/apps/details?id=com.agentaway.beta&hl=en \n\nDownload the app and start accepting the tasks. Join the world of business! \n\n\nRegards\n'+this.user.name+"\n"+this.user.RoleName;
     }
   

   
  onSubmit() {
    var msg = ''
    if (this.emails.length == 0) {
      msg = msg + 'Please enter emails.<br />'
    }
    if (this.subject == '') {
      msg = msg + 'Please enter subject.<br />'
    }
    if (this.msg == '') {
      msg = msg + 'Please enter message.'
    }
          
    if (msg != '') {
      this.tools.openAlert(msg);
    } else {
    
      console.log(this.emails);   
    var form1={
      message : this.msg,
      subject:  this.subject,
      Email:  Array.prototype.map.call(this.emails, function(item) { return item.value; }).join(","),
    }
   console.log(form1);
   this.tools.openLoader();
   this.commonService.inviteFriends(form1).then(data => {
     this.tools.closeLoader();   
     this.tools.openAlert(JSON.parse(data.data).message);
     this.emails= [];
     this.subject="Agent Away | Invitation to Join";
     
     this.msg='Hi, \nI am pleased to invite you to join my team on Agent Away using the code mentioned below.\n\n'+this.user.RoleName+' Code - '+this.user.my_broker_code+'\n\niOS users - https://apps.apple.com/us/app/apple-store/id1477325592 \n\nAndroid users -  https://play.google.com/store/apps/details?id=com.agentaway.beta&hl=en \n\nDownload the app and start accepting the tasks. Join the world of business! \n\n\nRegards\n'+this.user.name+"\n"+this.user.RoleName;

   }).catch(error => {
     console.log(error);
     this.tools.closeLoader();
     if (error && error.error) {
       this.tools.openAlert(JSON.parse(error.error).message);
     }
   });
  }
  }
  ngOnInit() {
  }
  edit(){
    this.isEdit=true;
  }

}
