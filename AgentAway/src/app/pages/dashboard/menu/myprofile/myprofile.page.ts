import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { ActionSheetController, Events } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { IonicSelectableComponent } from 'ionic-selectable';
import { States } from 'src/app/types/States.type';
import { City } from 'src/app/types/City.type';
import { Subscription } from 'rxjs';
import { StateService } from 'src/app/shared/country.services';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.page.html',
  styleUrls: ['./myprofile.page.scss'],
})
export class MyprofilePage implements OnInit {

  @ViewChild('stateComponent') stateComponent: IonicSelectableComponent;
  @ViewChild('cityComponent') cityComponent: IonicSelectableComponent;

  
states: States[];
state1: States;
cities: City[];
city1:City;
stateSubscription: Subscription;
citySubscription: Subscription;

businessCard=false;
  profileForm: FormGroup;
  image1: string='';
  image2: string='';
  isEdit=false;
  zipcodesList: any = [];


//   resData=[];
// cityData=[];
//   
// cityData1={CityID:'', CityName: ''};
// stateData={
//   StateID:'',StateName:'',City:[]
// };
public asyncValidators = [this.validateAsync];
public validators = [this.startsWithAt];
selectCity:any;

  user:any;
  constructor(public router: Router,
    private stateService: StateService,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public actionSheetController: ActionSheetController,
    private camera: Camera,
    public events: Events,
    public tools: Tools) {
      this.user=commonService.getUserData();
      this.setData();
     }
     pickImage(sourceType) {
      const options: CameraOptions = {
        quality: 30,
        sourceType: sourceType,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then((imageData) => {
        console.log('User Image --> ',imageData);
       this.image1 = imageData;
    //    this.storage.set('userimage',imageData);
      }, (err) => {
        console.log(err);
      });
    }
    pickImage2(sourceType) {
      const options: CameraOptions = {
        quality: 30,
        sourceType: sourceType,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then((imageData) => {
         this.image2 =  imageData;
        //  this.storage.set('business_card',imageData);
      }, (err) => {
        console.log(err);
      });
    }
    async selectImage(type) {
      const actionSheet = await this.actionSheetController.create({
        header: "Select Image",
        buttons: [{
          text: 'Load from Library',
          handler: () => {
            if(type=='1'){
              this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
            }else{
              this.pickImage2(this.camera.PictureSourceType.PHOTOLIBRARY);
              }
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            if(type=='1'){
            this.pickImage(this.camera.PictureSourceType.CAMERA);
          }else{
            this.pickImage2(this.camera.PictureSourceType.CAMERA);
            }
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    }
    ionViewDidEnter(){
      this.tools.callOneSignal();
      this.profileForm.get('device_token').setValue(localStorage.getItem('PlearID'));
      
      for (let i = 0; i < this.stateService.states.length; i++) {
        const element = this.stateService.states[i];
        if(element.StateName == this.profileForm.get('state').value){
          this.state1=element;
          for (let j = 0; j < element.City.length; j++) {
            const element1 = element.City[j];
            if(element1.CityName == this.profileForm.get('city').value){
              this.city1=element1;
            }
          }      
        }        
      }
    }
   
    filterState(ports: States[], text: string): any[] {
      return ports.filter(port => {
        return port.StateName.toLowerCase().indexOf(text) !== -1;
      });
    }
    filterCity(ports: City[], text: string ): any[] {
      return ports.filter(port => {
        return port.CityName.toLowerCase().indexOf(text) !== -1 &&  port;
      });
    }
    searchState(event: {
      component: IonicSelectableComponent,
      text: string
    }) {
      let text = event.text.trim().toLowerCase();
      event.component.startSearch();
      this.profileForm.get('city').setValue("");
      this.city1={CityID:'0',CityName:''}
  
  
      if (this.citySubscription) {
        this.citySubscription.unsubscribe();
      }
      if (this.stateSubscription) {
        this.stateSubscription.unsubscribe();
      }
  
      if (!text) {
        // Close any running subscription.
        if (this.stateSubscription) {
          this.stateSubscription.unsubscribe();
        }
  
        event.component.items = [];
        event.component.endSearch();
        return;
      }
  
      this.stateSubscription = this.stateService.getStatesAsync().subscribe(states => {
        // Subscription will be closed when unsubscribed manually.
        if (this.stateSubscription.closed) {
          return;
        }
  
        event.component.items = this.filterState(states, text);
        event.component.endSearch();
      });
    }
    searchCity(event: {
      component: IonicSelectableComponent,
      text: string
    }) {
    //  this.clear();
    
      let text = event.text.trim().toLowerCase();
      event.component.startSearch();
  
      // Close any running subscription.
      if (this.citySubscription) {
        this.citySubscription.unsubscribe();
      }
  
      if (!text) {
        // Close any running subscription.
        if (this.citySubscription) {
          this.citySubscription.unsubscribe();
        }
  
        event.component.items = [];
        event.component.endSearch();
        return;
      }
  
      this.citySubscription = this.stateService.getCityAsync(this.state1.StateID).subscribe(city => {
        // Subscription will be closed when unsubscribed manually.
        if (this.citySubscription.closed) {
          return;
        }
  
        event.component.items = this.filterCity(city, text);
        event.component.endSearch();
      });
    }
  
  onSubmit(form: NgForm) {
    var msg = ''
    if (this.profileForm.get('name').value == '') {
      msg = msg + 'Please enter name.<br />'
    }
    if (this.profileForm.get('mobile_no').value == '') {
      msg = msg + 'Please enter Mobile no.<br />'
    }
    if (this.profileForm.get('email').value == '') {
      msg = msg + 'Please enter email.<br />'
    }
    if (this.profileForm.get('license_number').value == '') {
      msg = msg + 'Please enter license number.<br />'
    }
    if (this.profileForm.get('brokeroffice_name').value == '') {
      msg = msg + 'Please enter broker office name.<br />'
    }
    if (this.profileForm.get('street_address').value == '') {
      msg = msg + 'Please enter address.<br />'
    }
   
    if (this.state1 == undefined || this.state1.StateName =='') {
      msg = msg + 'Please select state.<br />'
    }
    if (this.city1 == undefined || this.city1.CityName =='') {
      msg = msg + 'Please select city.<br />'
    }
    if (this.profileForm.get('st_zip').value == '') {
      msg = msg + 'Please enter Zip Code.<br />'
    }
     if ( this.zipcodesList.length == 0 ) {
      msg = msg + 'Please enter zip code Areas of interest.<br />';
    }      
    if (msg != '') {
      this.tools.openAlert(msg);
    } else {
      console.log('Zip Code List ',this.zipcodesList);
    var form1={
      name: this.profileForm.get('name').value,
        license_number:  this.profileForm.get('license_number').value,
        mobile_no:  this.profileForm.get('mobile_no').value,
        email:  this.profileForm.get('email').value,
        userimage:  this.image1,
        business_card:  this.image2,
        brokeroffice_name:  this.profileForm.get('brokeroffice_name').value,
        nardsid:  this.profileForm.get('nardsid').value,
        my_broker_code: this.user.my_broker_code,
        broker_code: this.user.broker_code,
        street_address:  this.profileForm.get('street_address').value,
        about:  this.profileForm.get('about').value,
        desvehiclesafety:  this.profileForm.get('desvehiclesafety').value,
        city: this.city1.CityName, //this.registerForm.get('city').value,
          state: this.state1.StateName, //this.registerForm.get('state').value,
        st_zip:  this.profileForm.get('st_zip').value,
        zipcodes:  Array.prototype.map.call(this.zipcodesList, function(item) { return item.value; }).join(","),// this.profileForm.get('ZipCode').value,
        role_id:  this.profileForm.get('role_id').value,
        notify_text:  this.profileForm.get('notify_text').value,
        notify_email:  this.profileForm.get('notify_email').value,
        notify_phone: this.profileForm.get('notify_phone').value,
        device_token:  this.profileForm.get('device_token').value,
        IsShowTwentyFour: this.user.IsShowTwentyFour,
        ethicalcodeofconduct:  this.profileForm.get('ethicalcodeofconduct').value==false?'0':'1',
        nonecompate:  this.profileForm.get('nonecompate').value==false?'0':'1',
        saftey: this.profileForm.get('saftey').value==false?'0':'1',
        econsert:  this.profileForm.get('econsert').value==false?'0':'1',
    }
 
   console.log('Sample Data --> ',form1);
   this.tools.openLoader();
    this.commonService.updateProfile(form1).then(data => {
      this.tools.closeLoader();
      this.commonService.setUserData(JSON.parse(data.data).data.user, '');
      console.log('Sample Data2 --> ',JSON.parse(data.data).data.user);
      this.tools.openAlert(JSON.parse(data.data).message);
      this.isEdit=false;
      this.image1='';
      this.image2='';
      this.user.userimage=this.commonService.getUserData().userimage;
      this.user.userimage=this.commonService.getUserData().business_card;
      this.events.publish('imgChange', this.user,this.user.userimage); 
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  }
  ngOnInit() {
    this.tools.callOneSignal();
    console.log('Push Token --> ',localStorage.getItem('PlearID'));
    this.profileForm.get('device_token').setValue(localStorage.getItem('PlearID'));
  
  }
   ionViewWillEnter(){
  
    this.tools.callOneSignal();
  console.log('Push Token --> ',localStorage.getItem('PlearID'));
  this.profileForm.get('device_token').setValue(localStorage.getItem('PlearID'));
  }
  edit(){
    this.isEdit=true;
  }
  private validateAsync(control: FormControl): Promise<any> {
    return new Promise(resolve => {
        const value = control.value;
        const result: any = isNaN(value) ? {
            isNan: true
        } : null;

        setTimeout(() => {
            resolve(result);
        }, 1);
    });
}
private startsWithAt(control: FormControl) {
  if (control.value.length >5) {
      return {
          'startsWithAt@': true
      };
  }

  return null;
}

public asyncErrorMessages = {
  'startsWithAt@': 'zip code length maximum 5 digits',
  isNan: 'Please only add numbers'
};

imagePreview(imgPAth){
  this.businessCard= !this.businessCard
console.log(imgPAth);
}

setData(){
  console.log(this.user.ZiipCode);

for (let i = 0; i < this.user.ZiipCode.length; i++) {
  const element = this.user.ZiipCode[i];
   console.log('ass ==> ',element.ZipCode);
   this.zipcodesList.push({display: element.ZipCode, value: element.ZipCode} );
}

  this.profileForm = this.formBuilder.group({
    name: [this.user.name, [Validators.required]],
    license_number: [this.user.license_number, [Validators.required]],
    mobile_no: [this.user.mobile_no, [Validators.required,Validators.maxLength(10)]],
    email: [this.user.email],
    userimage: [this.user.userimage],
    business_card: [this.user.business_card],
    brokeroffice_name: [this.user.brokeroffice_name, [Validators.required]],
    nardsid: [this.user.nardsid],
    my_broker_code: [this.user.my_broker_code],
    broker_code: [this.user.broker_code],
    street_address: [this.user.street_address, [Validators.required]],
    about: [this.user.About],
    desvehiclesafety: [this.user.DesVehicleSafety],
    st_zip: [this.user.st_zip, [Validators.required,Validators.maxLength(5)]],
    city: [this.user.city],
    state: [this.user.state],
    zipcodes:[Array.prototype.map.call(this.zipcodesList, function(item) { return item.value; }).join(",")],
    role_id: this.user.role_id,
    notify_text: this.user.notify_text,
    notify_email: this.user.notify_email,
    notify_phone:this.user.notify_phone,
    device_token: localStorage.getItem('PlearID'),
    IsShowTwentyFour: this.user.IsShowTwentyFour,
    ethicalcodeofconduct: this.user.ethicalcodeofconduct=="0"?false:true,
    nonecompate: this.user.nonecompate=="0"?false:true,
    saftey:this.user.saftey=="0"?false:true,
    econsert: this.user.econsert=="0"?false:true,
  });
}
}
