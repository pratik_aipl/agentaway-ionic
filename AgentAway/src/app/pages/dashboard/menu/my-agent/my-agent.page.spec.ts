import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAgentPage } from './my-agent.page';

describe('MyAgentPage', () => {
  let component: MyAgentPage;
  let fixture: ComponentFixture<MyAgentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyAgentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAgentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
