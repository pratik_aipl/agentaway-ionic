import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-my-agent',
  templateUrl: './my-agent.page.html',
  styleUrls: ['./my-agent.page.scss'],
})
export class MyAgentPage implements OnInit {
  agentList: any = [];
  constructor(public router: Router,
    public alertController: AlertController,
    public commonService: CommonService,
    public tools: Tools) {
    
  }
  ionViewDidEnter(){
    this.getAgentList();
  }
  ngOnInit() {
    
  }
  getAgentList() {
    this.tools.openLoader();
    this.commonService.agentList().then(data => {
      this.tools.closeLoader();
      if (JSON.parse(data.data).data != undefined)
        this.agentList = JSON.parse(data.data).data.AgentList;
      console.log(this.agentList);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }

  viewProfile(item){
    console.log('Agent Item --> ',item);
    this.router.navigateByUrl('dashboard/profile/'+item.id);
  }
  action(item, status) {
   
    var form = { Status: status, id: item.id }

      if(item.AgentStatus !=0 && status === 2){

        this.presentAlert('Are you sure you want to remove agent from your team?',item, status,form)
      }else{
        this.tools.openLoader();
        this.commonService.agentUpdate(form).then(data => {
          this.tools.closeLoader();
          this.tools.openAlert(JSON.parse(data.data).message);
          for (let i = 0; i < this.agentList.length; i++) {
            const element = this.agentList[i];
            if (element.id == item.id) {
              if (status == '1') {
                this.agentList[i].AgentStatus = status;
              }
              else {
                this.agentList.splice(i, 1);
              }
              break;
            }
          }
        }).catch(error => {
          this.tools.closeLoader();
          console.log('error.error',error.error)
          if (error && error.error) {
            console.log('parse ',JSON.parse(error.error))
    
            if(JSON.parse(error.error).member_limit){
              if(JSON.parse(error.error).member_limit == 1){    
                this.tools.presentAwailableTask(JSON.parse(error.error).message,'Subscribe','Cancel')
              }else{
                this.tools.openAlert(JSON.parse(error.error).message);
              }
            }else{
              this.tools.openAlert(JSON.parse(error.error).message);
            }
          }
        });
      }
    


  }

async presentAlert(message,item, status,form) {
        const alert = await this.alertController.create({
            message: message,
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {

                    }
                },
                {
                    text:'Yes',
                    handler: () => {
                      this.tools.openLoader();
                      this.commonService.agentUpdate(form).then(data => {
                        this.tools.closeLoader();
                        this.tools.openAlert(JSON.parse(data.data).message);
                        for (let i = 0; i < this.agentList.length; i++) {
                          const element = this.agentList[i];
                          if (element.id == item.id) {
                            if (status == '1') {
                              this.agentList[i].AgentStatus = status;
                            }
                            else {
                              this.agentList.splice(i, 1);
                            }
                            break;
                          }
                        }
                      }).catch(error => {
                        this.tools.closeLoader();
                        console.log('error.error',error.error)
                        if (error && error.error) {
                          console.log('parse ',JSON.parse(error.error))
                  
                          if(JSON.parse(error.error).member_limit){
                            if(JSON.parse(error.error).member_limit == 1){    
                              this.tools.presentAwailableTask(JSON.parse(error.error).message,'Subscribe','Cancel')
                            }else{
                              this.tools.openAlert(JSON.parse(error.error).message);
                            }
                          }else{
                            this.tools.openAlert(JSON.parse(error.error).message);
                          }
                        }
                      });
                    }
                }
            ], backdropDismiss: true
        });
        return await alert.present();
    }
}
