import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-agent-details',
  templateUrl: './agent-details.page.html',
  styleUrls: ['./agent-details.page.scss'],
})
export class AgentDetailsPage implements OnInit {

  agent:any={};
  agentId:any;
  constructor(
    private activatedRoute: ActivatedRoute,
    public commonService: CommonService,) {
      
    this.agentId = this.activatedRoute.snapshot.paramMap.get('agentId');
    this.agent={
      name:'Devid Scott'
    }

    console.log('Agent Id --> ',this.agentId);
    
   }

  ngOnInit() {
  }

}
