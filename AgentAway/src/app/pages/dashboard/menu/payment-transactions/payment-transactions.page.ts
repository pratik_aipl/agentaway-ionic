import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-payment-transactions',
  templateUrl: './payment-transactions.page.html',
  styleUrls: ['./payment-transactions.page.scss'],
})
export class PaymentTransactionsPage implements OnInit {
  selectPage:any;
  paymentRecivedList=[];
  paidOutList=[];
  transactionList=[];
  constructor(public router: Router,public commonService:CommonService,
    public events: Events,private tools:Tools) { 
      this.selectPage='post';
    // events.subscribe('taskUpdate', (item) => {
    //   console.log('Event call',item)
    //   this.PaymentRecived(false);
    // });

  }

  ionViewDidEnter(){
    this.PaymentRecived(true);
  }

  ngOnInit() {
   
  }

  openChat(item,status){
    this.commonService.selectedTask(item);
    this.router.navigateByUrl('dashboard/message-details/'+item.name+'/'+item.AcceptedBy+'/list');
  }
  

  PaymentRecived(isShow) {
    if(isShow)
    this.tools.openLoader();
    this.commonService.PaymentRecived().then(data => {
      this.tools.closeLoader();
      this.paymentRecivedList=[];
      console.log( ' PaymentRecived ',JSON.parse(data.data));
      if(JSON.parse(data.data).data != undefined)
      this.paymentRecivedList = JSON.parse(data.data).data.my_accepted_task;
      this.PaidOut(isShow);
      // for (let i = 0; i < this.transactionList.length; i++) {
      //   const element = this.transactionList[i];
      //   if(element.CreatedBy == this.commonService.getUserId()){
      //       this.paidOutList.push(element);
      //   }else{
      //     this.paymentRecivedList.push(element);
      //   }
        
      // }

      console.log('Payment Recived --> ', this.paymentRecivedList);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }

  PaidOut(isShow) {
    if(isShow)
    this.tools.openLoader();
    this.commonService.PaidOut().then(data => {
      this.tools.closeLoader();
      this.paidOutList=[];
      console.log( ' PaidOut ',JSON.parse(data.data));
      if(JSON.parse(data.data).data != undefined)
      this.paidOutList = JSON.parse(data.data).data.my_accepted_task;
      // for (let i = 0; i < this.transactionList.length; i++) {
      //   const element = this.transactionList[i];
      //   if(element.CreatedBy == this.commonService.getUserId()){
      //       this.paidOutList.push(element);
      //   }else{
      //     this.paymentRecivedList.push(element);
      //   }
        
      // }

      console.log('Paid Out --> ', this.paidOutList);
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }


  taskClick(item,type){
    this.commonService.selectedTask(item);
    this.commonService.selectedTask(item);
    this.router.navigateByUrl('/task-details/'+type+'/pay');
    
  }

   viewProfile(id){
    console.log('Task User ID '+id)
    this.router.navigateByUrl('dashboard/profile/'+id);
    }
}
