import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  notifyForm: FormGroup;
  user: any;
  constructor(public router: Router, private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) {
    this.user = commonService.getUserData();
    this.notifyForm = this.formBuilder.group({
      notify_text: [this.user.notify_text == '1' ? true : false, Validators.required],
      notify_email: [this.user.notify_email == '1' ? true : false],
      IsShowTwentyFour: [this.user.IsShowTwentyFour == '1' ? true : false],
      notify_phone: [this.user.notify_phone == '1' ? true : false],
    });
  }

  ngOnInit() {
  }
  onSubmit(form: NgForm) {
    if (this.notifyForm.get('notify_text').value || this.notifyForm.get('notify_email').value || this.notifyForm.get('notify_phone').value) {
      var form1 = {
        name: this.user.name,
        mobile_no: this.user.mobile_no,
        email:  this.user.email,
        license_number: this.user.license_number,
        brokeroffice_name: this.user.brokeroffice_name,
        street_address: this.user.street_address,
        city: this.user.city,
        state:  this.user.state,
        st_zip: this.user.st_zip,
        ZipCode: this.user.ZipCode,        
        role_id: this.user.role_id,
        my_broker_code: this.user.my_broker_code,
        broker_code: this.user.broker_code,
                about: [this.user.About],
        desvehiclesafety: [this.user.DesVehicleSafety],
        notify_text: this.notifyForm.get('notify_text').value?'1':'0',
        notify_email: this.notifyForm.get('notify_email').value?'1':'0',
        notify_phone: this.notifyForm.get('notify_phone').value?'1':'0',
        IsShowTwentyFour: this.notifyForm.get('IsShowTwentyFour').value?'1':'0',
        device_token: this.user.device_token,
        ethicalcodeofconduct: this.user.ethicalcodeofconduct,
        nonecompate: this.user.nonecompate,
        saftey:this.user.saftey,
        econsert: this.user.econsert,
      }

      this.tools.openLoader();
      this.commonService.updateProfile(form1).then(data => {
        console.log(data);
        this.tools.closeLoader();
        this.commonService.setUserData(JSON.parse(data.data).data.user, '');
        this.tools.openAlert(JSON.parse(data.data).message);
      }).catch(error => {
        console.log(error);
        this.tools.closeLoader();
        if (error && error.error) {
          this.tools.openAlert(JSON.parse(error.error).message);
        }
      });
    } else {
      this.tools.openAlert('Please check any one type of notify.');
    }
  }
}
