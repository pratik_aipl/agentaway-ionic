import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuPage } from './menu.page';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: MenuPage
  },
];

@NgModule({
  entryComponents: [ ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage,]
})
export class MenuPageModule {}
