import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { FormGroup, FormBuilder, NgForm } from '@angular/forms';
import { ModalController, PopoverController, AlertController } from '@ionic/angular';
import { PayModalComponent } from './pay-model/pay-model.component';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.page.html',
  styleUrls: ['./subscribe.page.scss'],
})
export class SubscribePage {
  subscribePlan: any;
  planHistoryList: any = [];
  planList: any = [];
  active_plan: any;
  isActive=false;
  isShowHistory=false;
  selectPlan:any;
  // subscribeForm: FormGroup;
  user: any;
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public modalController: ModalController,
    public alertController: AlertController,
    public tools: Tools) {
    // this.subscribeForm = this.formBuilder.group({
    //   subscribe: [this.user.activeplan_id != '0' ? true : false],
    // });
   this.callReloadData();
  }

  ionViewDidEnter(){
    this.commonService.userDetails();
    this.user=this.commonService.getUserData();
    if((this.user.role_id =='3' || this.user.role_id =='5')){
      this.isShowHistory=true;
    }else{
      this.isShowHistory=false;
    }
    this.checkAvtivation();
  }


  callReloadData(){
    this.getPlanList();
    this.planHistory();
  }
  async clickToSubScribe(plan) {
    console.log('Click To Subscribe '+plan.massage);
    console.log('Click To Subscribe '+parseInt(plan.available_member));

    const alert = await this.alertController.create({
      // title:'',
      header: plan.PlanPrice+'/'+plan.Taskmessage,
        message: plan.massage,
        buttons: [
            {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                    // console.log('Cancel clicked');
                }
            },
            {
                text: 'Pay Now',
                handler: () => {
                  this.callPay(plan);   
                }
            }
        ], cssClass: 'alertConfirm', backdropDismiss: true 
    });

    if(this.active_plan != undefined){
      if(parseInt(this.active_plan.plan_available_member) > parseInt(plan.available_member) ){
        if((parseInt(this.active_plan.plan_available_member)-parseInt(this.active_plan.available_member)) <= parseInt(plan.available_member)){
          return await alert.present();
        }else{
          this.tools.openAlert('Please remove the members from your team in order to match with the limit of the plan.');
        }
      }else{
        return await alert.present();
      }

    }else{
      return await alert.present();
    }


    
    
  }
  async callPay(plan){
      // this.tools.openAlert('Under Developement.');
        const modal = await this.modalController.create({
          component: PayModalComponent,
          cssClass: 'modal',
          componentProps: { value: plan.PlanPrice }
        });
        await modal.present();
        await modal.onDidDismiss()
          .then((data) => {
            console.log(data);
            if (data.data) {
              //this.answer.answers[this.currquekey].file = data.data;
              this.tools.openLoader();
              var form = { PlanID: plan.PlanID,TransactionID:data.data }
              this.commonService.subScribePlan(form).then((data) => {
                this.tools.closeLoader();
                console.log('subscribe data',data);
                //this.tools.openAlert(JSON.parse(data.data).message);
                // this.user.activeplan_id = plan.PlanID;
                // this.commonService.setUserData(this.user, '');
                this.isActive =true;
                 this.callReloadData();
              }).catch(error => {
                console.log(error);
                this.tools.closeLoader();
                if (error && error.error) {
                  this.tools.openAlert(JSON.parse(error.error).message);
                }
              });
            }
          });
      
  }
  getPlanList() {
    this.commonService.getPlan().then(data => {
      this.planList = JSON.parse(data.data).data.plan_list;
      console.log(this.planList);
    });
  }
  planHistory() {
    this.tools.openLoader();
    this.commonService.getPlanHistory().then(data => {
      this.tools.closeLoader();
      console.log(data.data);

      this.planHistoryList = JSON.parse(data.data).data.plan_history;
      if(JSON.parse(data.data).data.active_plan !=null)
      this.active_plan = JSON.parse(data.data).data.active_plan;
      console.log(this.active_plan.available_tasks);
     
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }

  checkAvtivation(){

    console.log('Rol ID ',this.commonService.getUserData().role_id);
  
    console.log('Active Status 0 ' +this.user.available_tasks);
    
      if((this.user.role_id =='4' || this.user.role_id =='6')){
   
      console.log('Active Status 1');
      if(this.commonService.getUserData().AgentStatus !='0'){
        console.log('Agent Status ',this.commonService.getUserData().AgentStatus);
        console.log('Active Status 2');
        if(parseInt(this.user.available_tasks) !=0 && parseInt(this.user.available_member)!=0 ){
          console.log('Active Status 3');
          this.isActive = true;
        }else if(parseInt(this.user.available_tasks)== 0 || parseInt(this.user.available_member) == 0 ){
          console.log('Active Status 33');
          this.isActive = false;
        } else{
          console.log('Active Status 4');
          this.isActive = false;
        }  
      }else{
        console.log('Active Status 5');
        this.isActive = true;
      }
    }else{
     
      console.log('Active Status 6 -> '+ parseInt(this.user.available_tasks));
      console.log('Active Status 6 -> '+ parseInt(this.user.available_member));
      if(parseInt(this.user.available_tasks)!= 0 && parseInt(this.user.available_member) != 0 ){
        console.log('Active Status 7');
        this.isActive = true;
      }else if(parseInt(this.user.available_tasks)== 0 || parseInt(this.user.available_member) == 0 ){
        console.log('Active Status 77');
        this.isActive = false;
      }      else{
        console.log('Active Status 8');
        this.isActive = false;
      }  
    }
    console.log('Active Status ',this.isActive);
  }
}
