import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Stripe } from '@ionic-native/stripe/ngx';
import { Tools } from 'src/app/tools';
import { environment } from 'src/environments/environment';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-pay-model',
  templateUrl: './pay-model.component.html',
  styleUrls: ['./pay-model.component.scss'],
})
export class PayModalComponent implements OnInit {
  cardForm: FormGroup;
  amt:any;
  amtNew:any;
  cardDetails:any={};
  constructor(public navParams: NavParams,
    public formBuilder: FormBuilder,private stripe: Stripe, public tools: Tools,
    public commonServices: CommonService,public modalCtrl: ModalController) {

      this.amt = this.navParams.get('value');
      this.amtNew = this.amt.replace("$","");
      this.amtNew =  Math.round(parseFloat(this.amtNew)*100);
 
      console.log('Passed Amount',this.amtNew);

    this.cardForm = this.formBuilder.group({
      cardnumber: ['', [Validators.required,Validators.pattern('^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$')]],
      cardexpiry: ['', [Validators.required,Validators.pattern('^(0[1-9]|1[0-2])\/?(([0-9]{4}|[0-9]{2})$)')]],
      cardcvv: ['', [Validators.required]],
    });
  }
  ngOnInit() { }

  callStrip(){

    var str  = this.cardForm.get('cardcvv').value;
   
    console.log(' card Number ',this.amt);
    console.log(' card Number ',this.amt.replace("$",""));
    console.log(' card Number ',this.cardForm.get('cardnumber').value);
    console.log(' cardcvv ',str);

    var dateValue=this.cardForm.get('cardexpiry').value;
    this.stripe.setPublishableKey(environment.stripPublishableKey);
    let month= dateValue.split('/')[0]+''; //this.cardForm.get('cardcvv').value.slice(1,2)
    let year= dateValue.split('/')[1].slice(-2); //this.cardForm.get('cardcvv').value.slice(3,5);

    console.log(' month ',month);
    console.log(' year ',year);
    console.log(' cvv ',str);

    this.cardDetails = {
      number: this.cardForm.get('cardnumber').value,
      expMonth: month,
      expYear: year,
      cvc: str
    }
    this.tools.openLoader();
    this.stripe.createCardToken(this.cardDetails)
      .then(token => {
        console.log(token.id);
        this.makePayment(token.id);
      })
      .catch(error =>{ 
        console.error(error);
        this.tools.closeLoader();
        this.tools.openAlert(error);
      });   
  }

  makePayment(token) {
    let paydata={
      'amount':this.amtNew,
      'currency':'usd',
      'source':token,
      'description':'test pay'
    }

    this.commonServices.makePayment(paydata).then(data => {
      this.tools.closeLoader();
      this.modalCtrl.dismiss(JSON.parse(data.data).balance_transaction);
      // this.commonServices.setBookedPass(JSON.parse(data.data).data);
      // this.router.navigateByUrl('/');//confirm-bookings
    }).catch(error => {
      console.log(error);
      if (error) {
        this.tools.openAlert(error);
      } else if (error) {
        this.tools.openAlert(error);
      }
      this.tools.closeLoader();
    });
     }
  dismissModal() {
    // environment.isBeaconModalOpen = false;
   // this.modalCtrl.dismiss('');
  }
  cancel() {
    // environment.isBeaconModalOpen = false;
    this.modalCtrl.dismiss('');
  }
}
