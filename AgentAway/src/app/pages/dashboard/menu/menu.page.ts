import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Tools } from 'src/app/tools';
import { CommonService } from 'src/app/shared/common.service';
import { ModalController, Events } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  user:any;
  constructor(public router: Router,public tools:Tools,  public events: Events,
     public modalController: ModalController,public commonSerice:CommonService) {
      events.subscribe('imgChange', (item, newImage) => {
        this.user= item;
        console.log('Event call',newImage)
      });
      }

  ngOnInit() {
    this.user=this.commonSerice.getUserData();
    console.log('init --> ',this.user.userimage);
  }
  ionViewDidEnter(){
    this.commonSerice.userDetails();
    this.user=this.commonSerice.getUserData();
  }
  opneProfile(){
    this.router.navigateByUrl('myprofile');
    
  }
  gotoAgent(){
    this.router.navigateByUrl('dashboard/my-agent');
  }
  inviteFriend(){
    this.router.navigateByUrl('invite-friend');
  }
  opneSettings(){
    this.router.navigateByUrl('dashboard/settings');
  }
  opneSubscribe(){
    this.router.navigateByUrl('dashboard/subscribe');
    // this.router.navigateByUrl('subscribe');
  }
  opneBankDetails(){
    this.router.navigateByUrl('/bank-details/menu');
  }
  paymentTransactions(){
    this.router.navigateByUrl('dashboard/payment-transactions');
  }

  opneChangePassword(){
    this.router.navigateByUrl('dashboard/change-password');
  }
  faq(){
    this.router.navigateByUrl('dashboard/faq');
  }
  contactus(){
    this.router.navigateByUrl('dashboard/contact-us');
  }
  logout(){
    this.tools.presentLogout('Are you sure you want to logout?','Logout','Cancel')
  }
}
