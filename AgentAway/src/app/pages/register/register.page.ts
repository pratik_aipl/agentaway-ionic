import { IonicSelectableComponent } from 'ionic-selectable';
import { City } from './../../types/City.type';
import { States } from './../../types/States.type';
import { Component, OnInit,  ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, NgForm, FormControl } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { AlertController, ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { rawdata } from 'src/environments/rawdata';
import { Storage } from '@ionic/storage';
import { Subscription } from 'rxjs';
import { StateService } from 'src/app/shared/country.services';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})


export class RegisterPage implements OnInit {

  @ViewChild('stateComponent') stateComponent: IonicSelectableComponent;
  @ViewChild('cityComponent') cityComponent: IonicSelectableComponent;

  
states: States[];
state1: States;
cities: City[];
city1:City;
stateSubscription: Subscription;
citySubscription: Subscription;

pos:any;
setState=false;
  signuptype: any;
  regesterType: any;
  registerForm: FormGroup;

  verificationId: any;
  code: string = "";
  phoneNumber: string = "";

  image1: string;
  image2: string;
  teamList = [];
  // file: any;
  // images: any;
  ethicalcodeofconduct=false;
  nonecompate=false;
  saftey=false;
  econsert=false;
  terms=false;
    zipcodes: any = [];
    statesSubscription: Subscription;
  htmlString: any;
  public asyncValidators = [this.validateAsync];
  public validators = [this.startsWithAt];

  
  clear() {
    this.cityComponent.clear();
  }
 
  constructor(public router: Router, private activatedRoute: ActivatedRoute,
    private stateService: StateService,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    private alertCtrl: AlertController,
    private storage: Storage,
    public actionSheetController: ActionSheetController,
    private camera: Camera,
    public tools: Tools) {

      this.htmlString = 'If you don\'t know your NARDS click here: <br /> <a href="https://login.connect.realtor/#!/forgotmember" > https://login.connect.realtor/#!/forgotmember</a>'

    this.signuptype = this.activatedRoute.snapshot.paramMap.get('selectedtype');
    if (this.signuptype == 'Realtor') {
      this.regesterType = '2';
    } else if (this.signuptype == 'Broker') {
      this.regesterType = '3';
    } else {
      this.regesterType = '5';
    }
    if (this.signuptype != 'Realtor') {
      this.teamList = this.commonService.getTypeUSerList();
    }

    this.registerForm = this.formBuilder.group({
      selectedTeam: [''],
      name: [''],
      mobile_no: ['',[Validators.required, Validators.maxLength(10)]],
      email: ['', [Validators.required, Validators.email.bind(this)]],
      password: [''],
      cpwd: [''],
      license_number: [''],
      brokeroffice_name: [''],
      street_address: [''],
      about: [''],
      desvehiclesafety: [''],
      nardsid: [''],
      userimage: [''],
      business_card: [''],
      city: [''],
      st_zip: ['',[Validators.required, Validators.maxLength(5)]],
      state: [''],
      zipcodes: [''],
      role_id: [this.regesterType],
      role_name: [this.signuptype],
      notify_text: [true],
      notify_email: [false],
      notify_phone: [false],
      ethicalcodeofconduct:[false],
      nonecompate:[false],
      saftey:[false],
      econsert:[false],
      terms:[false],
      device_token: [localStorage.getItem('PlearID')],
    });
  }
  ionViewWillEnter(){
    this.tools.callOneSignal();
    console.log('Push Token --> ',localStorage.getItem('PlearID'));
    this.registerForm.get('device_token').setValue(localStorage.getItem('PlearID'));
    // this.platform.ready().then(() => {      
    //   this.callOneSignal();
    // });
  }
  filterState(ports: States[], text: string): any[] {
    return ports.filter(port => {
      return port.StateName.toLowerCase().indexOf(text) !== -1;
    });
  }
  filterCity(ports: City[], text: string ): any[] {
    return ports.filter(port => {
      return port.CityName.toLowerCase().indexOf(text) !== -1 &&  port;
    });
  }
  searchState(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();
    this.registerForm.get('city').setValue("");
    this.city1={CityID:'0',CityName:''}


    if (this.citySubscription) {
      this.citySubscription.unsubscribe();
    }
    if (this.stateSubscription) {
      this.stateSubscription.unsubscribe();
    }

    if (!text) {
      // Close any running subscription.
      if (this.stateSubscription) {
        this.stateSubscription.unsubscribe();
      }

      event.component.items = [];
      event.component.endSearch();
      return;
    }

    this.stateSubscription = this.stateService.getStatesAsync().subscribe(states => {
      // Subscription will be closed when unsubscribed manually.
      if (this.stateSubscription.closed) {
        return;
      }

      event.component.items = this.filterState(states, text);
      event.component.endSearch();
    });
  }
  searchCity(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
  //  this.clear();
  
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    // Close any running subscription.
    if (this.citySubscription) {
      this.citySubscription.unsubscribe();
    }

    if (!text) {
      // Close any running subscription.
      if (this.citySubscription) {
        this.citySubscription.unsubscribe();
      }

      event.component.items = [];
      event.component.endSearch();
      return;
    }

    this.citySubscription = this.stateService.getCityAsync(this.state1.StateID).subscribe(city => {
      // Subscription will be closed when unsubscribed manually.
      if (this.citySubscription.closed) {
        return;
      }

      event.component.items = this.filterCity(city, text);
      event.component.endSearch();
    });
  }

  ngOnInit() {

  }
  
  ionViewDidEnter(){
  //  this.cityStateData();
  }
  cityStateData() {
    this.tools.openLoader();
    this.commonService.getCityState().then(data => {
      this.tools.closeLoader();
    console.log('City State Data ',JSON.parse(data.data).data);
    this.states= JSON.parse(data.data).data.state;
    this.cities= JSON.parse(data.data).data.city;

    // for (let i = 0; i < this.states.length; i++) {
    //   for (let j = 0; j < this.states[i].City.length; j++) {       
    //     this.cities.push({CityID:this.states[i].City[j].CityID,CityName:this.states[i].City[j].CityName});      
    //   }
    // }

    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  async selectImage(type) {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          if(type=='1'){
            this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
          }else{
            this.pickImage2(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          if(type=='1'){
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }else{
          this.pickImage2(this.camera.PictureSourceType.CAMERA);
          }
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  async stateSelected(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    // this.registerForm.get  
    // this.registerForm.get('city').setValue('');
    this.registerForm.get('state').setValue(event.value.StateName);
    
    //  this.cities=[];
    //  this.city1={CityID:0,CityName:''}
//     var size = 30;
    //  this.cities=event.value.City.slice(0,size);
   //  this.cities=event.value.City;
    //  this.cities.push(event.value.City.slice(0,size));
      // for (var i=30; i<event.value.City.length; i+=size) {
      //   // console.log('City Data :--> ', event.value.City.slice(i,i+size));
      //     this.cities.push(event.value.City.slice(i,i+size));
      // }

  }
  async citySelected(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    console.log('port:', event.value);
    this.registerForm.get('city').setValue(event.value.CityName);
  }
  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 30,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
//   console.log('User Image --> ',imageData);
     this.image1 = imageData;
      this.storage.set('userimage',imageData);
    }, (err) => {
      console.log(err);
    });
  }

  pickImage2(sourceType) {
    const options: CameraOptions = {
      quality: 30,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
       this.image2 =  imageData;
       this.storage.set('business_card',imageData);
    }, (err) => {
      console.log(err);
    });
  }

  async presentConfirm(header,message,type) {
    const  alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: [
        // {
        //   text: 'Cancel',
        //   role: 'cancel',
        //   handler: () => {
        //     console.log('Cancel clicked');
        //   }
        // },
        {
          text: 'Accept',
          handler: () => {
            //console.log('Buy clicked');
            if (type == '0') {
              this.ethicalcodeofconduct = true;
            } else if (type == '1') {
              this.nonecompate = true;
            } else if (type == '2') {
              this.saftey = true;
            } else if (type == '3') {
              this.econsert = true;
            } else if (type == '4') {
              this.terms = true;
            }
          }
        }
      ]
    });
    await alert.present();
  }

  selectType(event) {
    this.regesterType = event.detail.value;
  }
  labelClick(event,type) {
    console.log(event.target.innerHTML, type)
    if(type != '-1'){
      if(type==0){
        this.presentConfirm(event.target.innerHTML,rawdata.EthicalCodeofConduct,type);
      }else if(type==1){
        this.presentConfirm(event.target.innerHTML,rawdata.NonCompete,type);
      }else if(type==2){
        this.presentConfirm(event.target.innerHTML,rawdata.Safety,type);
      }else if(type==3){
        this.presentConfirm(event.target.innerHTML,rawdata.ElectronicSignatureConsent,type);
      }else if(type==4){
        this.presentConfirm('Terms & Conditions',rawdata.termsData1+rawdata.termsData2+rawdata.termsData3+rawdata.termsData4+rawdata.termsData5+rawdata.termsData6+rawdata.termsData7,type);
      }
    }
  }

  clickBack() {
     this.storage.clear();
    this.router.navigateByUrl('/');
  }

  onSubmit(form: NgForm) {
    var msg = ''
    if (this.registerForm.get('name').value == '') {
      msg = msg + 'Please enter name.<br />'
    }
    if (this.registerForm.get('mobile_no').value == '') {
      msg = msg + 'Please enter mobile no.<br />'
    }
    if (this.registerForm.get('email').value == '') {
      msg = msg + 'Please enter email.<br />'
    }

    if (this.registerForm.get('password').value == '') {
      msg = msg + 'Please enter password.<br />'
    }

    if (this.registerForm.get('cpwd').value == '') {
      msg = msg + 'Please enter confirm password.<br />'
    }
    if (this.registerForm.get('license_number').value == '') {
      msg = msg + 'Please enter license number.<br />'
    }
    if (this.registerForm.get('brokeroffice_name').value == '') {
      msg = msg + 'Please enter broker office name.<br />'
    }
    if (this.registerForm.get('nardsid').value == '') {
      msg = msg + 'Please enter NARDS ID.<br />'
    }
    if (this.registerForm.get('street_address').value == '') {
      msg = msg + 'Please enter address.<br />'
    }
    if (this.state1 == undefined || this.state1.StateName =='') {
      msg = msg + 'Please select state.<br />'
    }
    if (this.city1 == undefined || this.city1.CityName =='') {
      msg = msg + 'Please select city.<br />'
    }
    if (this.registerForm.get('st_zip').value == '') {
      msg = msg + 'Please enter Zip Code.<br />'
    }
    if ( this.zipcodes.length == 0 ) {
      msg = msg + 'Please enter zip code Areas of interest.<br />';
    }
    // if (this.registerForm.get('state').value == '') {
    //   msg = msg + 'Please enter Zip code Areas of interest.'
    // }          

    if (msg != '') {
      this.tools.openAlert(msg);
    } else {
      if (this.registerForm.get('password').value == this.registerForm.get('cpwd').value) {
        if (this.ethicalcodeofconduct) {
          this.CallSec();
        }else{
          this.presentConfirm('Ethical Code Of Conduct',rawdata.EthicalCodeofConduct,0);
        }    
      } else {
        this.tools.openAlert('Password and Confirm password do not match.');
      }
    }
   
  }
  private validateAsync(control: FormControl): Promise<any> {
    return new Promise(resolve => {
        const value = control.value;
        const result: any = isNaN(value) ? {
            isNan: true
        } : null;

        setTimeout(() => {
            resolve(result);
        }, 1);
    });
    
}

private startsWithAt(control: FormControl) {
  if (control.value.length >5) {
      return {
          'startsWithAt@': true
      };
  }

  return null;
}

public asyncErrorMessages = {
  'startsWithAt@': 'zip code length maximum 5 digits',
  isNan: 'Please only add numbers'
};

  CallSec(){
    if (this.nonecompate) {
      this.CallThird()
    }else{
      this.presentConfirm('Non Compete',rawdata.NonCompete,1);
    }
  }

  CallThird(){
    if (this.saftey) {
      this.CallForth();
    }else{
      this.presentConfirm('Saftey',rawdata.Safety,2);
    }
  }
  CallForth(){
    if (this.econsert) {
      this.CallFinal();
    }else{
      this.presentConfirm('E Consert',rawdata.ElectronicSignatureConsent,3);
    }
  }

  CallFinal() {
    if ((this.regesterType == '4' || this.regesterType == '6') && this.registerForm.get('selectedTeam').value == "") {
      this.tools.openAlert('Please enter your '+this.signuptype+' Code');
    } else {
      this.callData();
    }
  }

  callData(){
    if (this.terms) {
      if (this.registerForm.get('notify_text').value || this.registerForm.get('notify_email').value || this.registerForm.get('notify_phone').value) {
        var form2 = {
          name: this.registerForm.get('name').value,
          license_number: this.registerForm.get('license_number').value,
          mobile_no: this.registerForm.get('mobile_no').value,
          email: this.registerForm.get('email').value,
          userimage: '',
          business_card: '',
          password: this.registerForm.get('password').value,
          brokeroffice_name: this.registerForm.get('brokeroffice_name').value,
          nardsid: this.registerForm.get('nardsid').value,
          street_address: this.registerForm.get('street_address').value,
          about: this.registerForm.get('about').value,
          desvehiclesafety: this.registerForm.get('desvehiclesafety').value,
          city: this.city1.CityName, //this.registerForm.get('city').value,
          state: this.state1.StateName, //this.registerForm.get('state').value,
          st_zip: this.registerForm.get('st_zip').value,
          ZipCode: Array.prototype.map.call(this.zipcodes, function(item) { return item.value; }).join(","),
          role_id: this.regesterType,
          notify_text: this.registerForm.get('notify_text').value ? '1' : '0',
          notify_email: this.registerForm.get('notify_email').value ? '1' : '0',
          notify_phone: this.registerForm.get('notify_phone').value ? '1' : '0',
          ethicalcodeofconduct: this.ethicalcodeofconduct ? '1' : '0',
          nonecompate: this.nonecompate ? '1' : '0',
          saftey: this.saftey ? '1' : '0',
          econsert: this.econsert ? '1' : '0',
          device_token:this.registerForm.get('device_token').value,
          subcode: this.registerForm.get('selectedTeam').value
        }

        console.log(form2);
       this.tools.openLoader();
        var form1 = { mobile_no: this.registerForm.get('mobile_no').value, 
        device_token: this.registerForm.get('device_token').value 
        , broker_code: this.registerForm.get('selectedTeam').value,
        role_id:this.regesterType
      }
        this.commonService.send_otp(form1).then(data => {
          console.log('response -->  ',JSON.parse(data.data).data.mobile_no);
          console.log('response -->  ',JSON.parse(data.data).data.otp);
         this.tools.closeLoader();
          this.commonService.setTempRegister(form2);
          // this.router.navigateByUrl('/otpvarify/' + this.registerForm.get('mobile_no').value + '/123456' );
          this.router.navigateByUrl('/otpvarify/' + JSON.parse(data.data).data.mobile_no + '/' + JSON.parse(data.data).data.otp);
        }).catch(error => {
          console.log('response --> error ',error);
          this.tools.closeLoader();
          if (error && error.error) {
            this.tools.openAlert(JSON.parse(error.error).message);
          }
        });
      } else {
        this.tools.openAlert('Please check any one type of notify.');
      }
    } else {
      this.presentConfirm('Terms & Conditions',rawdata.TermsAndConditions+rawdata.TermsAndConditions1+rawdata.TermsAndConditions2+rawdata.TermsAndConditions3+rawdata.TermsAndConditions4,4);
    }
  }
  goLogin() {
    this.router.navigateByUrl('/');
  }
}
