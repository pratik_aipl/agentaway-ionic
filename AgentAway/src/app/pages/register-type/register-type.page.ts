import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Tools } from 'src/app/tools';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-register-type',
  templateUrl: './register-type.page.html',
  styleUrls: ['./register-type.page.scss'],
})
export class RegisterTypePage implements OnInit {
  selectedRadio:any;
  constructor(public router: Router,
    public commonService: CommonService,
    public tools: Tools) {
   }

  ngOnInit() {
  }

  onChangeHandler($event) {
    this.selectedRadio = $event.target.value;
    console.log(this.selectedRadio);
  }
  clickRadio(selectType){
    // this.getAgentList(selectType);
    this.router.navigateByUrl('/register/'+selectType);
  }
  // getAgentList(selectType){
  //   if(selectType == 'Realtor'){
  //     this.router.navigateByUrl('/register/'+selectType);
  //   }else{
  //     this.tools.openLoader();  
  //     this.commonService.getTypeUserList(selectType=='Broker'?'broker':'team').then(data => {
  //       this.tools.closeLoader();  
  //       console.log(JSON.parse(data.data).data.Team_list);  
  //       this.commonService.setTypeUSerList(JSON.parse(data.data).data.Team_list); 
  //       this.router.navigateByUrl('/register/'+selectType);
  //     }).catch(error => {
  //       console.log(error);
  //       this.tools.closeLoader();
  //       if (error && error.error) {
  //         this.tools.openAlert(JSON.parse(error.error).message);
  //       }
  //     });
  //   }
    
  // }
  clickBack() {
    this.router.navigateByUrl('/');
  }
}
