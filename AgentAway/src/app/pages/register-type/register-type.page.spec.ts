import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterTypePage } from './register-type.page';

describe('RegisterPage', () => {
  let component: RegisterTypePage;
  let fixture: ComponentFixture<RegisterTypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterTypePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
