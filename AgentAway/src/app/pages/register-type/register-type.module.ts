import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterTypePage } from './register-type.page';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: RegisterTypePage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegisterTypePage]
})
export class RegisterTypePageModule {}
