import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools ) {
      // this.loginForm = this.formBuilder.group({
      //   mobile_no: ['9514611400', [Validators.required]],
      //   password: ['123456', Validators.required],
      //   device_token: ['gvdfgdfg', Validators.required],
      // });
      this.tools.callOneSignal();
      this.loginForm = this.formBuilder.group({
        mobile_no: ['', [Validators.required,Validators.maxLength(10)]],
        password: ['', Validators.required],
        device_token: [localStorage.getItem('PlearID')==''?"111111":localStorage.getItem('PlearID')],
      });
      
     }
     ionViewDidEnter(){
      this.tools.callOneSignal();
      this.loginForm.get('device_token').setValue(localStorage.getItem('PlearID')==''?"111111":localStorage.getItem('PlearID'));
    }
     ionViewWillEnter(){    
      this.tools.callOneSignal();
    console.log('Push Token --> ',localStorage.getItem('PlearID'));
    this.loginForm.get('device_token').setValue(localStorage.getItem('PlearID')==''?"111111":localStorage.getItem('PlearID'));
    }

  ngOnInit() {
  }
  onSubmit(form: NgForm) {
    console.log(form);

    var form1={mobile_no: this.loginForm.get('mobile_no').value, 
    password:this.loginForm.get('password').value , device_token:localStorage.getItem('PlearID')==''?"111111":localStorage.getItem('PlearID')}
    this.tools.openLoader();
    this.commonService.login(form1).then(data => {
      this.tools.closeLoader();   
      this.loginForm.reset();
      this.commonService.setUserData('','')  
      this.commonService.setUserData(JSON.parse(data.data).data.user,JSON.parse(data.data).data.login_token);
      // if(JSON.parse(data.data).data.user.BankingStatus == 1 ){
        this.router.navigateByUrl('/dashboard');
      // }else{
      //   this.router.navigateByUrl('/bank-details/login');
      // }
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  goToRegister(){
    this.router.navigateByUrl('/register-type');
  }
  goToForgotPassword(){
    this.router.navigateByUrl('/forgot-password');
  }
}
