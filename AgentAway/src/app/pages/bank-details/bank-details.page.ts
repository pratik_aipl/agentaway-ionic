import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Events, NavController } from '@ionic/angular';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.page.html',
  styleUrls: ['./bank-details.page.scss'],
})
export class BankDetailsPage implements OnInit {
  from:any;
  PageTitle='My Account Details';
  btnTitle='';
  btnClick = false;
  isEdit=false;
  isShow=false;
  BankName='';
  BankHolderName='';
  BankAccountNo='';
  BankIFSCCode='';
  Rounting='';
  SSN='';
  showInfoData = false;
  isNew=false;

//  bank-details
  constructor(public router: Router,
    private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,
    public events: Events,
    public commonService: CommonService,
    public tools: Tools ) {
    this.from = this.activatedRoute.snapshot.paramMap.get('from');
    this.BankIFSCCode='000000'
    }
  ngOnInit() {
  }
  ionViewDidEnter(){
   
   
    if(this.commonService.getUserData().BankingStatus ==undefined || this.commonService.getUserData().BankingStatus == "0" ){
      this.isEdit=true;
      this.isNew=true;
      this.btnTitle='Submit';
    }else{
      this.isNew=false;
      if(this.from == 'menu'){
        this.btnTitle='Save';
        this.getBankDetails();
      }else{
        this.isEdit=true;
        this.btnTitle='Submit';
      }
    }
    
  }
  getBankDetails(){
    this.tools.openLoader();
    this.commonService.bankDetails().then(data => {
      this.tools.closeLoader();
      console.log('Bank Details ',data);
      console.log('Bank Details ',data.data);
      console.log('Bank Details ',JSON.parse(data.data));
      var bankDetails =JSON.parse(data.data).data;
      console.log('Bank Details ',bankDetails);
      this.BankName=bankDetails.BankName;
      this.BankHolderName=bankDetails.BankHolderName;
      this.BankAccountNo=bankDetails.BankAccountNo;
      this.BankIFSCCode=bankDetails.BankIFSCCode;
      this.Rounting=bankDetails.Rounting;
      this.SSN=bankDetails.SSN;
      // this.events.publish('taskUpdate');
      // this.navController.navigateRoot('/dashboard/tabs/mypostedtask');
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
  edit(){
    this.isEdit=true;
  }
  showInfo(){
    this.showInfoData = !this.showInfoData
  }
  onSubmit() {
    var msg = ''
    if (this.BankName == '') {
      msg = msg + 'Please enter bank name.<br />';
    } if (this.BankHolderName == '') {
      msg = msg + 'Please enter bank holder name.<br />';
    } if (this.BankAccountNo == '') {
      msg = msg + 'Please enter valid bank account no.<br />';
    } if (this.BankIFSCCode == '') {
      msg = msg + 'Please enter bank ifsc code. <br />';
    } 
    if (this.Rounting == '') {
      msg = msg + 'Please enter routing no.<br />';
    } 
    if (this.SSN == '') {
      msg = msg + 'Please enter ssn.';
    } 
    if (msg != '') {
      this.tools.openAlert(msg);
    } else {
      var form1 = {
        BankName: this.BankName,
        BankHolderName: this.BankHolderName,
        BankAccountNo: this.BankAccountNo,
        BankIFSCCode: this.BankIFSCCode,         
        Rounting: this.Rounting,         
        SSN: this.SSN,         
      }
       if (!this.isNew) {
        console.log('Updated Data', form1);
        this.tools.openLoader();
        this.commonService.updateBanking(form1).then(data => {
          this.tools.closeLoader();
          this.isEdit=false;
          // this.events.publish('taskUpdate');
          // this.navController.navigateRoot('/dashboard/tabs/mypostedtask');
        }).catch(error => {
          console.log(error);
          this.tools.closeLoader();
          if (error && error.error) {
            this.tools.openAlert(JSON.parse(error.error).message);
          }
        });
      } else {
        if (!this.btnClick) {
          this.tools.openLoader();
          console.log('Updated Data', form1);
          this.btnClick = true;
            this.commonService.addBanking(form1).then(data => {
              this.btnClick = false;
              this.tools.closeLoader();
               var user = this.commonService.getUserData();
               user.BankingStatus = "1" 
               this.commonService.setUserData(user, '');
              this.router.navigateByUrl('/dashboard');
            }).catch(error => {
              console.log(error);
              this.btnClick = false;
              this.tools.closeLoader();
              if (error && error.error) {
                    this.tools.openAlert(JSON.parse(error.error).message);
              }
            });
          
        }
      }
    }
  }
}
