import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { BankDetailsPage } from './bank-details.page';

const routes: Routes = [
  {
    path: '',
    component: BankDetailsPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BankDetailsPage]
})
export class BankDetailsPageModule {}
