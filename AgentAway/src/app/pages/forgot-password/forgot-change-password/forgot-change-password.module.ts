import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SharedModule } from 'src/app/shared/shared.module';
import { ForgotChangePasswordPage } from './forgot-change-password.page';

const routes: Routes = [
  {
    path: '',
    component: ForgotChangePasswordPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ForgotChangePasswordPage]
})
export class ForgotChangePasswordPageModule {}
