import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, NgForm, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-forgot-change-password',
  templateUrl: './forgot-change-password.page.html',
  styleUrls: ['./forgot-change-password.page.scss'],
})
export class ForgotChangePasswordPage implements OnInit {

  cpForm: FormGroup;
  mobileno:any;
  constructor(public router: Router,private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) {

      this.mobileno = this.activatedRoute.snapshot.paramMap.get('mobileno');
      this.cpForm = this.formBuilder.group({
        new_password: ['', Validators.required],
        Confirm: ['', Validators.required],
      });
     }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log(form);

    var form1 = { mobile_no: this.mobileno , 
    password: this.cpForm.get('new_password').value 
      }
    
    if(this.cpForm.get('new_password').value != this.cpForm.get('Confirm').value){
      this.tools.openAlert('New Password And Confirm Password Dont Match.');
    }else{
      this.tools.openLoader();
      this.commonService.forgotPassword(form1).then(data => {
        this.tools.closeLoader();    
        this.cpForm.reset(); 
        // this.tools.openAlert(JSON.parse(data.data).message);
        this.tools.presentForgot(JSON.parse(data.data).message,'Login');
  
      }).catch(error => {
        console.log(error);
        this.tools.closeLoader();
        if (error && error.error) {
          this.tools.openAlert(JSON.parse(error.error).message);
        }
      });
    }

    
  }
}
