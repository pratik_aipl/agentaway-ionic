import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Tools } from 'src/app/tools';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  loginForm: FormGroup;
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) {
      this.loginForm = this.formBuilder.group({
        mobile_no: ['', [Validators.required]],
        device_token: ['123456'],
      });
     }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    this.tools.openLoader();    

    var form1 = { mobile_no: this.loginForm.get('mobile_no').value, 
        device_token: this.loginForm.get('device_token').value 
      }

    this.commonService.forgotPasswordNew(form1).then(data => {
      console.log(data);
      this.tools.closeLoader();      
//      this.loginForm.get('mobile_no').value
      this.router.navigateByUrl('/forgot-otp/' + JSON.parse(data.data).data.mobile_no + '/' + JSON.parse(data.data).data.otp);    
      // this.tools.presentForgot(JSON.parse(data.data).message,'Login');
    }).catch(error => {
      console.log(error.error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });

  }
  clickBack() {
    this.router.navigateByUrl('/');
  }

}
