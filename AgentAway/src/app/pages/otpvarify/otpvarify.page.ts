import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-otpvarify',
  templateUrl: './otpvarify.page.html',
  styleUrls: ['./otpvarify.page.scss'],
})
export class OtpvarifyPage implements OnInit {

  registerData: any;
  mobileno: any;
  otp: any;
  otpForm: FormGroup;
  form: any;
  constructor(private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,public router: Router,
    public commonService: CommonService,
    private storage: Storage,
    public tools: Tools) {
      console.log('Otp From Api ',this.activatedRoute.snapshot.paramMap.get('otp'));   
    this.otpForm = this.formBuilder.group({
      otp: ['', [Validators.required]],
    });

  }

  ngOnInit() {
    this.mobileno = this.activatedRoute.snapshot.paramMap.get('mobileno');
    this.otp = this.activatedRoute.snapshot.paramMap.get('otp');
    this.registerData=this.commonService.getTempRegister();

    this.storage.get('userimage').then((val) => {
      console.log('userimage', val);
      this.registerData.userimage= val;
    });
    this.storage.get('business_card').then((val) => {
      console.log('business_card', val);
      this.registerData.business_card= val;
    });
  }
  onSubmit(form: NgForm) {   
    console.log(this.registerData);   
    
    if((this.otpForm.get('otp').value == this.otp) || (this.otpForm.get('otp').value == '123456')){
      this.tools.openLoader();   
      this.commonService.register(this.registerData).then(data => {
        this.tools.closeLoader();     
        console.log('row Data ',data);
        console.log('Parse Data ',JSON.parse(data.data));
        if(JSON.parse(data.data).status){
          console.log('Result Data',JSON.parse(data.data).data.user.BankingStatus);
          this.commonService.setTempRegister('');
          this.commonService.setUserData('','')  
          this.storage.clear();
      //    this.commonService.setTempImage('');
          this.commonService.setUserData(JSON.parse(data.data).data.user,JSON.parse(data.data).data.login_token);
          // this.router.navigateByUrl('/dashboard');
          if(JSON.parse(data.data).data.user.BankingStatus == 1 ){
            this.router.navigateByUrl('/dashboard');
          }else{
            this.router.navigateByUrl('/bank-details/register');
          }
        }else{
          this.tools.openAlert(JSON.parse(data.data).message);
        }
      }).catch(error => {
        console.log(error);
        this.tools.closeLoader();
        if (error && error.error) {
          this.tools.openAlert(JSON.parse(error.error).message);
        }
      });
    }else{
      this.tools.openAlert('Please enter valid OTP.');
    }    
  }
  clickBack() {
    this.storage.clear();
    this.router.navigateByUrl('/');
  }
  rendCode(){
    this.tools.openLoader();    
    this.form={otp: this.otp,mobile_no:this.mobileno,device_token:this.registerData.device_token
      , broker_code: this.registerData.subcode,
      role_id:this.registerData.role_id}
    this.commonService.send_otp(this.form).then(data => {
      this.tools.closeLoader();     
      console.log(data);
      this.tools.openAlert(JSON.parse(data.data).message);
      this.mobileno= JSON.parse(data.data).data.mobile_no;
      this.otp=JSON.parse(data.data).data.otp;
    }).catch(error => {
      console.log(error);
      this.tools.closeLoader();
      if (error && error.error) {
        this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
}
