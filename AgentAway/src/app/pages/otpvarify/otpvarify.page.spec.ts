import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtpvarifyPage } from './otpvarify.page';

describe('OtpvarifyPage', () => {
  let component: OtpvarifyPage;
  let fixture: ComponentFixture<OtpvarifyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtpvarifyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtpvarifyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
