import { Component, ViewChildren, QueryList } from '@angular/core';

import { Platform, ModalController, MenuController, ActionSheetController, PopoverController, IonRouterOutlet } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { Tools } from './tools';
import { OneSignal } from '@ionic-native/onesignal/ngx';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  lastTimeBackPress = 0;
  timePeriodToExit = 2000;

  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public modalCtrl: ModalController,
    private menu: MenuController,
    private actionSheetCtrl: ActionSheetController,
    private popoverCtrl: PopoverController,private oneSignal: OneSignal ) {
    this.initializeApp();
    this.backButtonEvent();
  }

  initializeApp() {

    const firebaseConfig = {
      apiKey: "AIzaSyDNF54DWGjeBmvo3fXUfZ0o6Xr7OC7yDyI",
      authDomain: "agentaway-e7006.firebaseapp.com",
      databaseURL: "https://agentaway-e7006.firebaseio.com",
      projectId: "agentaway-e7006",
      storageBucket: "agentaway-e7006.appspot.com",
      messagingSenderId: "531698865453",
      appId: "1:531698865453:web:5ab270bb6f9db999de2fa4",
      measurementId: "G-WTLPLJ9JF3"
    };
    firebase.initializeApp(firebaseConfig);


    this.platform.ready().then(() => {
      this.statusBar.overlaysWebView(true);
      // this.statusBar.backgroundColorByHexString('#ffffff');
      this.statusBar.show();
      this.splashScreen.hide();
      this.callOneSignal();
    });
    
  }
  callOneSignal() {
    this.oneSignal.startInit('056cab92-bf48-4ab2-a3d1-1ea7c0839778', '383797773178');

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    
    
    this.oneSignal.handleNotificationReceived().subscribe(() => {
     // do something when notification is received
    });
    
    this.oneSignal.handleNotificationOpened().subscribe(() => {
      // do something when a notification is opened
    });
    
    this.oneSignal.endInit();
    this.oneSignal.getIds().then((id) => {
      console.log('userId ==> ',id.userId);
      console.log('pushToken ==> ',id.pushToken);
      localStorage.setItem('PlearID',id.userId);
      
    });
  }

  // active hardware back button
  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {

    // close action sheet
    try {
      const element = await this.actionSheetCtrl.getTop();
      if (element) {
          element.dismiss();
          return;
      }
  } catch (error) {
  }

  // close popover
  try {
      const element = await this.popoverCtrl.getTop();
      if (element) {
          element.dismiss();
          return;
      }
  } catch (error) {
  }

  // close modal
  try {
      const element = await this.modalCtrl.getTop();
      if (element) {
          element.dismiss();
          return;
      }
  } catch (error) {
      console.log(error);

  }

  // close side menua
  try {
      const element = await this.menu.getOpen();
      if (element) {
          this.menu.close();
          return;

      }

  } catch (error) {

  }

      if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
        // this.platform.exitApp(); // Exit from app
        navigator['app'].exitApp(); // work in ionic 4

    } else {
     // this.tools.presentToast('Press back again to exit App.');
        this.lastTimeBackPress = new Date().getTime();
    }
    });
}

}
