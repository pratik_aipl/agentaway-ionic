import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { Camera } from '@ionic-native/Camera/ngx';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './shared/authguard.service';
import { Tools } from './tools';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from 'src/environments/environment.prod';
import { IonicStorageModule } from '@ionic/storage';
const config: SocketIoConfig = { url: environment.chatUrl, options: {} };
import { IonicSelectableModule } from 'ionic-selectable';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { StateService } from './shared/country.services';
import {NgxMaskIonicModule} from 'ngx-mask-ionic'
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
@NgModule({
  declarations: [AppComponent,],
  imports: [BrowserModule, BrowserAnimationsModule, IonicModule.forRoot({
    mode: 'md',
    scrollAssist: false
  }),AppRoutingModule,
  NgxMaskIonicModule.forRoot(),
  IonicSelectableModule,
  IonicStorageModule.forRoot({
    name: 'agentawaydb',
    driverOrder: ['indexeddb', 'sqlite', 'websql']
  }),
  SocketIoModule.forRoot(config)
],
  providers: [
    StatusBar,  InAppBrowser,
    SplashScreen,
    AuthGuard,
    HTTP,
    Tools,
    Camera,
    ImagePicker,
    OneSignal,
    StateService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
