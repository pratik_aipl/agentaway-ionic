import { ICity } from './ICity.interface';

export interface IStates {
    StateID: string;
    StateName: string;
    City?: ICity[];
  }