import { ICity } from './ICity.interface';

export class City implements ICity {
    CityID: string;
    CityName: string;

  constructor(city: ICity) {
    this.CityID = city.CityID;
    this.CityName = city.CityName;
  }
}