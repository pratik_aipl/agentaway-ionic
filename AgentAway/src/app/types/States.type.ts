import { IStates } from './IStates.interface';
import { ICity } from './ICity.interface';

export class States implements IStates {
    StateID: string="";
    StateName: string;
    City?: ICity[];

    constructor(state: IStates) {
      this.StateID = state.StateID;
      this.StateName = state.StateName;
      this.City = state.City;
    }
  }