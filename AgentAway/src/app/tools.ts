import { ToastController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@Injectable()
export class Tools {

    notification;
    loading;
    public creds: any;

    constructor(
        public alertController: AlertController,
        public toastController: ToastController,
        public loadingController: LoadingController,
        public router: Router,private platform: Platform,private oneSignal: OneSignal,
    ) {
        // console.log('TOOLS');
        this.creds = {
            bucket: 'iss-visit-files',
            bucketPolls: 'iss-polls',
            bucketExtendedProfile: 'isecretshop_shopper_files',
            access_key: 'AKIAIDW5OI677ZKZOTRA',
            secret_key: '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'
        };
    }
    callOneSignal() {
        this.oneSignal.startInit('056cab92-bf48-4ab2-a3d1-1ea7c0839778', '383797773178');
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.handleNotificationReceived().subscribe(() => {
         // do something when notification is received
        });
       
        this.oneSignal.handleNotificationOpened().subscribe(() => {
          // do something when a notification is opened
        });
        
        this.oneSignal.endInit();
        this.oneSignal.getIds().then((id) => {
          console.log('userId ==> ',id.userId);
          console.log('pushToken ==> ',id.pushToken);
          localStorage.setItem('PlearID',id.userId);
         // this.registerForm.get('device_token').setValue(id.userId);
        });
      }
    formatephone(phonenumber) {
        // tslint:disable-next-line:prefer-const
        let phonestring = phonenumber.replace(/[^a-zA-Z0-9]/g, '');
        // tslint:disable-next-line:prefer-const
        let formatednumber = phonestring.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
        return formatednumber;
    }


    async openNotification(options) {
        this.notification = await this.toastController.create({
            message: options.message,
            duration: options.duration ? options.duration : 3000,
            position: options.position ? options.position : 'top',
            cssClass: options.class ? options.class : 'info'
        });
        this.notification.present();
    }
    
    async presentToast(msg) {
        const toast = await this.toastController.create({
          message: msg,
          duration: 2000
        });
        toast.present();
      }
    async formateDateyyyymmddhis(date) {
        // tslint:disable-next-line:prefer-const
        let mm = ('0' + (date.getMonth() + 1)).slice(-2);
        // tslint:disable-next-line:prefer-const
        let dd = ('0' + (date.getDate())).slice(-2);
        // tslint:disable-next-line:prefer-const
        let yyyy = date.getFullYear();
        // tslint:disable-next-line:prefer-const
        let h = ('0' + (date.getHours())).slice(-2);
        // tslint:disable-next-line:prefer-const
        let i = ('0' + (date.getMinutes())).slice(-2);
        // tslint:disable-next-line:prefer-const
        let s = ('0' + (date.getSeconds())).slice(-2);
        // tslint:disable-next-line:prefer-const
        let today = yyyy + '-' + mm + '-' + dd + ' ' + h + ':' + i + ':' + s;
        return today;
    }

    async formateDay(today) {
        let dd = today.getDate();
        // tslint:disable-next-line:prefer-const
        let yyyy = today.getFullYear();
        let mm = today.getMonth() + 1; // January is 0!

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = yyyy + '-' + mm + '-' + dd;
        return today;
    }

    async formatAMPM1(date) {
        // console.log(date);
        let hours = date.getHours();
        let minutes = date.getMinutes();
        // tslint:disable-next-line:prefer-const
        let ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        // tslint:disable-next-line:prefer-const
        let strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    async formatAMPM(date) {
        let hours = date.getUTCHours();
        let minutes = date.getUTCMinutes();
        // tslint:disable-next-line:prefer-const
        let ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        // tslint:disable-next-line:prefer-const
        let strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    async openLoader(cssClass = '', msg?) {
        // console.log(' ===> ',msg);
        this.loading = await this.loadingController.create({
            message: msg ? msg : 'Please wait...',
            keyboardClose: true,
            showBackdrop: true,
            cssClass: cssClass
        });
        await this.loading.present();
    }

    async openAlert(message) {
        const alert = await this.alertController.create({
            message: message ? message : 'This is an alert message.',
            buttons: ['OK'],
            backdropDismiss: true
        });
        return await alert.present();
    }
    async openAlertTODO(message) {
        const alert = await this.alertController.create({
            message: message ? message : 'This is an alert message.',
            // buttons: ['OK'],
            buttons: [{
                text:'OK',
            handler:()=>{
                this.router.navigateByUrl('/dashboard/tabs/myacceptedtask');
            }}],
            backdropDismiss: true
        });
        return await alert.present();
    }
    async presentAlert(title, msg, btnOk) {
        const alert = await this.alertController.create({
            header: title,
            message: msg,
            buttons: [
                {
                    text: btnOk,
                    handler: () => {
                        // console.log('Confirm Okay');
                    }
                }
            ]
        });
        await alert.present();
    }
    async presentForgot(message, btnYes) {
        const alert = await this.alertController.create({
            message: message,
            buttons: [
                {
                    text: btnYes ? btnYes : 'Yes',
                    handler: () => {
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                    }
                }
            ], backdropDismiss: true
        });
        return await alert.present();
    }
    async presentLogout(message, btnYes, btnNo) {
        const alert = await this.alertController.create({
            message: message,
            buttons: [
                {
                    text: btnNo ? btnNo : 'Cancel',
                    role: 'cancel',
                    handler: () => {

                    }
                },
                {
                    text: btnYes ? btnYes : 'Yes',
                    handler: () => {
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                    }
                }
            ], backdropDismiss: true
        });
        return await alert.present();
    }

    async updateBankingDetails(message, btnYes) {
        const alert = await this.alertController.create({
            message: message,
            buttons: [
               
                {
                    text: btnYes ? btnYes : 'Yes',
                    handler: () => {
                        // localStorage.clear();
                        // this.router.navigateByUrl('/login');
                        this.router.navigateByUrl('/bank-details/login');
                    }
                }
            ], backdropDismiss: true
        });
        return await alert.present();
    }


    async presentAwailableTask(message, btnYes, btnNo) {
        const alert = await this.alertController.create({
            message: message,
            buttons: [
                {
                    text: btnNo ? btnNo : 'Cancel',
                    role: 'cancel',
                    handler: () => {

                    }
                },
                {
                    text: btnYes ? btnYes : 'Yes',
                    handler: () => {
                        this.router.navigateByUrl('dashboard/subscribe');
                    }
                }
            ], backdropDismiss: true
        });
        return await alert.present();
    }

    closeLoader() {
        // setTimeout(() => {
            if (this.loading) {
                this.loading.dismiss();
            }
        // }, 500);
    }

    dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        let byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0) {
            byteString = atob(dataURI.split(',')[1]);
        } else {
            byteString = unescape(dataURI.split(',')[1]);
        }
        // separate out the mime component
        // tslint:disable-next-line:prefer-const
        let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        // write the bytes of the string to a typed array
        // tslint:disable-next-line:prefer-const
        let ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], {
            type: mimeString
        });
    }

    returnImageName(keys, file, blob) {
        let imagename;
        if (blob) {
          imagename = new Date().getTime() + keys + '.jpg';
          blob.lastModified = new Date();
          blob.name = imagename;
        } else {
          imagename = new Date().getTime() + keys + file.name;
        }
        imagename.replace(/_/g, ' ');
        imagename = imagename.replace(/\s+/g, '-');
        imagename = imagename.replace(/%20/g, '_');
        return imagename;
      }
      
}

